<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'App\Http\Controllers'], function () {
    /**
     * Home Routes
     */
    Route::get('/beranda', 'HomeController@index')->name('home.index');
    Route::get('/ucapan', [\App\Http\Controllers\HomeController::class, 'ucapan'])->name('home.ucapan');
    Route::get('/guruuser', [\App\Http\Controllers\HomeController::class, 'guru'])->name('home.guru');
    Route::get('/prestasiuser', [\App\Http\Controllers\HomeController::class, 'prestasi'])->name('home.prestasi');
    Route::get('/galeriuser', [\App\Http\Controllers\HomeController::class, 'galeri'])->name('home.galeri');
    Route::get('/fasilitasuser', [\App\Http\Controllers\HomeController::class, 'fasilitas'])->name('home.fasilitas');
    Route::get('/visimisiuser', [\App\Http\Controllers\HomeController::class, 'visimisi'])->name('home.visimisi');
    Route::get('/programuser', [\App\Http\Controllers\HomeController::class, 'program'])->name('home.program');
    Route::get('/kalenderuser', [\App\Http\Controllers\HomeController::class, 'kalender'])->name('home.kalender');
    Route::get('/pengumumanuser', [\App\Http\Controllers\HomeController::class, 'pengumuman'])->name('home.pengumuman');
    Route::get('/beritauser', [\App\Http\Controllers\HomeController::class, 'berita'])->name('home.berita');
    Route::get('/isiberita/{id_berita}', [\App\Http\Controllers\HomeController::class, 'isiberita'])->name('home.isiberita');
    Route::get('/organisasiuser', [\App\Http\Controllers\HomeController::class, 'organisasi'])->name('home.organisasi');
    Route::get('/eskuluser', [\App\Http\Controllers\HomeController::class, 'eskul'])->name('home.eskul');
    Route::get('/detaileskul/{id_eskul}', [\App\Http\Controllers\HomeController::class, 'detaileskul'])->name('home.detaileskul');
    Route::get('/kontak', [\App\Http\Controllers\HomeController::class, 'kontak'])->name('home.kontak');


    Route::group(['middleware' => ['guest']], function () {
        /**
         * Register Routes
         */
        Route::get('/register', 'RegisterController@show')->name('register.show');
        Route::post('/register', 'RegisterController@register')->name('register.perform');

        /**
         * Login Routes
         */
        Route::get('/login', 'LoginController@show')->name('login.show');
        Route::post('/login', 'LoginController@login')->name('login.perform');
    });

    Route::group(['middleware' => ['auth']], function () {
        /**
         * Logout Routes
         */
        Route::get('/', 'DashboardController@index')->name('layouts.dashboard');
        Route::get('/logout', 'LogoutController@perform')->name('logout.perform');

        Route::resource('/admin', \App\Http\Controllers\AdminController::class);
        Route::get('/admin', [\App\Http\Controllers\AdminController::class, 'index'])->name('admin.index');
        Route::get('/admin/create', [\App\Http\Controllers\AdminController::class, 'create'])->name('admin.create');
        Route::post('/admin', [\App\Http\Controllers\AdminController::class, 'store'])->name('admin.store');
        Route::get('/admin/{id}/edit', [\App\Http\Controllers\AdminController::class, 'edit'])->name('admin.edit');
        Route::put('/admin/{id}', [\App\Http\Controllers\AdminController::class, 'update'])->name('admin.update');
        Route::delete('/admin/{id}', [\App\Http\Controllers\AdminController::class, 'destroy'])->name('admin.destroy');

        Route::resource('/berita', \App\Http\Controllers\BeritaController::class);
        Route::get('/berita', [\App\Http\Controllers\BeritaController::class, 'index'])->name('berita.index');
        Route::get('/berita/create', [\App\Http\Controllers\BeritaController::class, 'create'])->name('berita.create');
        Route::post('/berita', [\App\Http\Controllers\BeritaController::class, 'store'])->name('berita.store');
        Route::get('/berita/{id}/edit', [\App\Http\Controllers\BeritaController::class, 'edit'])->name('berita.edit');
        Route::put('/berita/{id}', [\App\Http\Controllers\BeritaController::class, 'update'])->name('berita.update');
        Route::delete('/berita/{id}', [\App\Http\Controllers\BeritaController::class, 'destroy'])->name('berita.destroy');

        Route::resource('/organisasi', \App\Http\Controllers\OrganisasiController::class);
        Route::get('/organisasi', [\App\Http\Controllers\OrganisasiController::class, 'index'])->name('organisasi.index');
        Route::get('/organisasi/create', [\App\Http\Controllers\OrganisasiController::class, 'create'])->name('organisasi.create');
        Route::post('/organisasi', [\App\Http\Controllers\OrganisasiController::class, 'store'])->name('organisasi.store');
        Route::get('/organisasi/{id}/edit', [\App\Http\Controllers\OrganisasiController::class, 'edit'])->name('organisasi.edit');
        Route::put('/organisasi/{id}', [\App\Http\Controllers\OrganisasiController::class, 'update'])->name('organisasi.update');
        Route::delete('/organisasi/{id}', [\App\Http\Controllers\OrganisasiController::class, 'destroy'])->name('organisasi.destroy');

        Route::resource('/eskul', \App\Http\Controllers\EskulController::class);
        Route::get('/eskul', [\App\Http\Controllers\EskulController::class, 'index'])->name('eskul.index');
        Route::get('/eskul/create', [\App\Http\Controllers\EskulController::class, 'create'])->name('eskul.create');
        Route::post('/eskul', [\App\Http\Controllers\EskulController::class, 'store'])->name('eskul.store');
        Route::get('/eskul/{id}/edit', [\App\Http\Controllers\EskulController::class, 'edit'])->name('eskul.edit');
        Route::put('/eskul/{id}', [\App\Http\Controllers\EskulController::class, 'update'])->name('eskul.update');
        Route::delete('/eskul/{id}', [\App\Http\Controllers\EskulController::class, 'destroy'])->name('eskul.destroy');


        Route::resource('/program', \App\Http\Controllers\ProgramSekolahController::class);
        Route::get('/program', [\App\Http\Controllers\ProgramSekolahController::class, 'index'])->name('program.index');
        Route::get('/program/create', [\App\Http\Controllers\ProgramSekolahController::class, 'create'])->name('program.create');
        Route::post('/program', [\App\Http\Controllers\ProgramSekolahController::class, 'store'])->name('program.store');
        Route::get('/program/{id}/edit', [\App\Http\Controllers\ProgramSekolahController::class, 'edit'])->name('program.edit');
        Route::put('/program/{id}', [\App\Http\Controllers\ProgramSekolahController::class, 'update'])->name('program.update');
        Route::delete('/program/{id}', [\App\Http\Controllers\ProgramSekolahController::class, 'destroy'])->name('program.destroy');

        Route::resource('/prestasi', \App\Http\Controllers\PrestasiController::class);
        Route::get('/prestasi', [\App\Http\Controllers\PrestasiController::class, 'index'])->name('prestasi.index');
        Route::get('/prestasi/create', [\App\Http\Controllers\PrestasiController::class, 'create'])->name('prestasi.create');
        Route::post('/prestasi', [\App\Http\Controllers\PrestasiController::class, 'store'])->name('prestasi.store');
        Route::get('/prestasi/{id}/edit', [\App\Http\Controllers\PrestasiController::class, 'edit'])->name('prestasi.edit');
        Route::put('/prestasi/{id}', [\App\Http\Controllers\PrestasiController::class, 'update'])->name('prestasi.update');
        Route::delete('/prestasi/{id}', [\App\Http\Controllers\PrestasiController::class, 'destroy'])->name('prestasi.destroy');

        Route::resource('/pengumuman', \App\Http\Controllers\PengumumanController::class);
        Route::get('/pengumuman', [\App\Http\Controllers\PengumumanController::class, 'index'])->name('pengumuman.index');
        Route::get('/pengumuman/create', [\App\Http\Controllers\PengumumanController::class, 'create'])->name('pengumuman.create');
        Route::post('/pengumuman', [\App\Http\Controllers\PengumumanController::class, 'store'])->name('pengumuman.store');
        Route::get('/pengumuman/{id}/edit', [\App\Http\Controllers\PengumumanController::class, 'edit'])->name('pengumuman.edit');
        Route::put('/pengumuman/{id}', [\App\Http\Controllers\PengumumanController::class, 'update'])->name('pengumuman.update');
        Route::delete('/pengumuman/{id}', [\App\Http\Controllers\PengumumanController::class, 'destroy'])->name('pengumuman.destroy');

        Route::resource('/galeri', \App\Http\Controllers\GaleriController::class);
        Route::get('/galeri', [\App\Http\Controllers\GaleriController::class, 'index'])->name('galeri.index');
        Route::get('/galeri/create', [\App\Http\Controllers\GaleriController::class, 'create'])->name('galeri.create');
        Route::post('/galeri', [\App\Http\Controllers\GaleriController::class, 'store'])->name('galeri.store');
        Route::get('/galeri/{id}/edit', [\App\Http\Controllers\GaleriController::class, 'edit'])->name('galeri.edit');
        Route::put('/galeri/{id}', [\App\Http\Controllers\GaleriController::class, 'update'])->name('galeri.update');
        Route::delete('/galeri/{id}', [\App\Http\Controllers\GaleriController::class, 'destroy'])->name('galeri.destroy');

        Route::resource('/guru', \App\Http\Controllers\GuruController::class);
        Route::get('/guru', [\App\Http\Controllers\GuruController::class, 'index'])->name('guru.index');
        Route::get('/guru/create', [\App\Http\Controllers\GuruController::class, 'create'])->name('guru.create');
        Route::post('/guru', [\App\Http\Controllers\GuruController::class, 'store'])->name('guru.store');
        Route::get('/guru/{id}/edit', [\App\Http\Controllers\GuruController::class, 'edit'])->name('guru.edit');
        Route::put('/guru/{id}', [\App\Http\Controllers\GuruController::class, 'update'])->name('guru.update');
        Route::delete('/guru/{id}', [\App\Http\Controllers\GuruController::class, 'destroy'])->name('guru.destroy');

        Route::resource('/fasilitas', \App\Http\Controllers\FasilitasController::class);
        Route::get('/fasilitas', [\App\Http\Controllers\FasilitasController::class, 'index'])->name('fasilitas.index');
        Route::get('/fasilitas/create', [\App\Http\Controllers\FasilitasController::class, 'create'])->name('fasilitas.create');
        Route::post('/fasilitas', [\App\Http\Controllers\FasilitasController::class, 'store'])->name('fasilitas.store');
        Route::get('/fasilitas/{id}/edit', [\App\Http\Controllers\FasilitasController::class, 'edit'])->name('fasilitas.edit');
        Route::put('/fasilitas/{id}', [\App\Http\Controllers\FasilitasController::class, 'update'])->name('fasilitas.update');
        Route::delete('/fasilitas/{id}', [\App\Http\Controllers\FasilitasController::class, 'destroy'])->name('fasilitas.destroy');

        Route::resource('/tentang', \App\Http\Controllers\TentangController::class);
        Route::get('/tentang', [\App\Http\Controllers\TentangController::class, 'index'])->name('tentang.index');
        Route::get('/tentang/create', [\App\Http\Controllers\TentangController::class, 'create'])->name('tentang.create');
        Route::post('/tentang', [\App\Http\Controllers\TentangController::class, 'store'])->name('tentang.store');
        Route::get('/tentang/{id}/edit', [\App\Http\Controllers\TentangController::class, 'edit'])->name('tentang.edit');
        Route::put('/tentang/{id}', [\App\Http\Controllers\TentangController::class, 'update'])->name('tentang.update');
        Route::delete('/tentang/{id}', [\App\Http\Controllers\TentangController::class, 'destroy'])->name('tentang.destroy');

        Route::resource('/kalender', \App\Http\Controllers\KalenderController::class);
        Route::get('/kalender', [\App\Http\Controllers\KalenderController::class, 'index'])->name('kalender.index');
        Route::get('/kalender/create', [\App\Http\Controllers\KalenderController::class, 'create'])->name('kalender.create');
        Route::post('/kalender', [\App\Http\Controllers\KalenderController::class, 'store'])->name('kalender.store');
        Route::get('/kalender/{id}/edit', [\App\Http\Controllers\KalenderController::class, 'edit'])->name('kalender.edit');
        Route::put('/kalender/{id}', [\App\Http\Controllers\KalenderController::class, 'update'])->name('kalender.update');
        Route::delete('/kalender/{id}', [\App\Http\Controllers\KalenderController::class, 'destroy'])->name('kalender.destroy');

        Route::resource('/ucapankepsek', \App\Http\Controllers\UcapanController::class);
        Route::get('/ucapankepsek', [\App\Http\Controllers\UcapanController::class, 'index'])->name('ucapankepsek.index');
        Route::get('/ucapankepsek/create', [\App\Http\Controllers\UcapanController::class, 'create'])->name('ucapankepsek.create');
        Route::post('/ucapankepsek', [\App\Http\Controllers\UcapanController::class, 'store'])->name('ucapankepsek.store');
        Route::get('/ucapankepsek/{id}/edit', [\App\Http\Controllers\UcapanController::class, 'edit'])->name('ucapankepsek.edit');
        Route::put('/ucapankepsek/{id}', [\App\Http\Controllers\UcapanController::class, 'update'])->name('ucapankepsek.update');
        Route::delete('/ucapankepsek/{id}', [\App\Http\Controllers\UcapanController::class, 'destroy'])->name('ucapankepsek.destroy');

        Route::resource('/visimisi', \App\Http\Controllers\VisimisiController::class);
        Route::get('/visimisi', [\App\Http\Controllers\VisimisiController::class, 'index'])->name('visimisi.index');
        Route::get('/visimisi/create', [\App\Http\Controllers\VisimisiController::class, 'create'])->name('visimisi.create');
        Route::post('/visimisi', [\App\Http\Controllers\VisimisiController::class, 'store'])->name('visimisi.store');
        Route::get('/visimisi/{id}/edit', [\App\Http\Controllers\VisimisiController::class, 'edit'])->name('visimisi.edit');
        Route::put('/visimisi/{id}', [\App\Http\Controllers\VisimisiController::class, 'update'])->name('visimisi.update');
        Route::delete('/visimisi/{id}', [\App\Http\Controllers\VisimisiController::class, 'destroy'])->name('visimisi.destroy');

        Route::resource('/sejarah', \App\Http\Controllers\SejarahController::class);
        Route::get('/sejarah', [\App\Http\Controllers\SejarahController::class, 'index'])->name('sejarah.index');
        Route::get('/sejarah/create', [\App\Http\Controllers\SejarahController::class, 'create'])->name('sejarah.create');
        Route::post('/sejarah', [\App\Http\Controllers\SejarahController::class, 'store'])->name('sejarah.store');
        Route::get('/sejarah/{id}/edit', [\App\Http\Controllers\SejarahController::class, 'edit'])->name('sejarah.edit');
        Route::put('/sejarah/{id}', [\App\Http\Controllers\SejarahController::class, 'update'])->name('sejarah.update');
        Route::delete('/sejarah/{id}', [\App\Http\Controllers\SejarahController::class, 'destroy'])->name('sejarah.destroy');

        Route::resource('/slider', \App\Http\Controllers\SliderController::class);
        Route::get('/slider', [\App\Http\Controllers\SliderController::class, 'index'])->name('slider.index');
        Route::get('/slider/create', [\App\Http\Controllers\SliderController::class, 'create'])->name('slider.create');
        Route::post('/slider', [\App\Http\Controllers\SliderController::class, 'store'])->name('slider.store');
        Route::get('/slider/{id}/edit', [\App\Http\Controllers\SliderController::class, 'edit'])->name('slider.edit');
        Route::put('/slider/{id}', [\App\Http\Controllers\SliderController::class, 'update'])->name('slider.update');
        Route::delete('/slider/{id}', [\App\Http\Controllers\SliderController::class, 'destroy'])->name('slider.destroy');

        Route::resource('/kalender', \App\Http\Controllers\KalenderController::class);
        Route::get('/kalender', [\App\Http\Controllers\KalenderController::class, 'index'])->name('kalender.index');
        Route::get('/kalender/create', [\App\Http\Controllers\KalenderController::class, 'create'])->name('kalender.create');
        Route::post('/kalender', [\App\Http\Controllers\KalenderController::class, 'store'])->name('kalender.store');
        Route::get('/kalender/{id}/edit', [\App\Http\Controllers\KalenderController::class, 'edit'])->name('kalender.edit');
        Route::put('/kalender/{id}', [\App\Http\Controllers\KalenderController::class, 'update'])->name('kalender.update');
        Route::delete('/kalender/{id}', [\App\Http\Controllers\KalenderController::class, 'destroy'])->name('kalender.destroy');

        Route::resource('/profil', \App\Http\Controllers\ProfilController::class);
        Route::get('/profil', [\App\Http\Controllers\ProfilController::class, 'index'])->name('profil.index');
        Route::get('/profil/create', [\App\Http\Controllers\ProfilController::class, 'create'])->name('profil.create');
        Route::post('/profil', [\App\Http\Controllers\ProfilController::class, 'store'])->name('profil.store');
        Route::get('/profil/{id}/edit', [\App\Http\Controllers\ProfilController::class, 'edit'])->name('profil.edit');
        Route::put('/profil/{id}', [\App\Http\Controllers\ProfilController::class, 'update'])->name('profil.update');
        Route::delete('/profil/{id}', [\App\Http\Controllers\ProfilController::class, 'destroy'])->name('profil.destroy');
    });
});

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class SejarahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::select(DB::raw("select * from sejarah"));
        return view('sejarah.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sejarah.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'keterangan' => 'required',
            'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        //upload image
            $file = $request->file('file');
            $file->storeAs('public/sejarah', $file->hashName());


            DB::insert("INSERT INTO `sejarah` (`id_sejarah`, `keterangan`, `file`) VALUES (uuid(), ?, ?)",
            [$request->keterangan,$file->hashName()]);
            return redirect()->route('sejarah.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('sejarah')->where('id_sejarah', $id)->first();
        return view('sejarah.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'keterangan' => 'required',
            'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);


        //cek update file
        if ($request->file('file')) {


            $file = $request->file('file');
            $file->storeAs('public/sejarah', $file->hashName());


            DB::update(
                "UPDATE `sejarah` SET `keterangan`=?,`file`=?  WHERE id_sejarah=?",
                [$request->keterangan,$file->hashName(), $id]
            );
        } else {
            DB::update(
                "UPDATE `sejarah` SET `keterangan`=? WHERE id_sejarah=?",
                [$request->keterangan, $id]
            );
        }
        return redirect()->route('sejarah.index')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('sejarah')->where('id_sejarah', $id)->delete();
        //redirect to index
        return redirect()->route('sejarah.index')->with(['success' => 'Data Berhasil Dihapus!']);
    }
}

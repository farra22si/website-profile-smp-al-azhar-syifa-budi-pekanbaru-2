<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class GuruController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::select(DB::raw("select * from guru"));
        return view('guru.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('guru.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_guru' =>'required',
            'jabatan' => 'required',
            'mapel' => 'required',
            'foto_guru' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        //upload image
            $foto_guru = $request->file('foto_guru');
            $foto_guru->storeAs('public/guru', $foto_guru->hashName());


            DB::insert("INSERT INTO `guru` (`id_guru`, `nama_guru`, `jabatan`, `mapel`, `foto_guru`) VALUES (uuid(), ?, ?, ?, ?)",
            [$request->nama_guru,$request->jabatan,$request->mapel,$foto_guru->hashName()]);
            return redirect()->route('guru.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('guru')->where('id_guru', $id)->first();
        return view('guru.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_guru' => 'required',
            'mapel' => 'required',
            'jabatan' => 'required',
            'foto_guru' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);


        //cek update foto
        if ($request->file('foto_guru')) {


            $foto_guru = $request->file('foto_guru');
            $foto_guru->storeAs('public/guru', $foto_guru->hashName());


            DB::update(
                "UPDATE `guru` SET `nama_guru`=?,`jabatan`=?,`mapel`=?,`foto_guru`=? WHERE id_guru=?",
                [ $request->nama_guru, $request->jabatan, $request->mapel, $foto_guru->hashName(), $id]
            );
        } else {
            DB::update(
                "UPDATE `guru` SET `nama_guru`=?,`jabatan`=?,`mapel`=? WHERE id_guru=?",
                [$request->nama_guru, $request->jabatan, $request->mapel, $id]
            );
        }
        return redirect()->route('guru.index')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('guru')->where('id_guru', $id)->delete();
        //redirect to index
        return redirect()->route('guru.index')->with(['success' => 'Data Berhasil Dihapus!']);
    }
}

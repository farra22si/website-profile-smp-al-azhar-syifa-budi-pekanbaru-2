<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class DashboardController extends Controller
{
    public function index()
    {
        $profil = DB::select(DB::raw("select * from profil"));
        $totalguru = DB::table('guru')->count();
        $totalprestasi = DB::table('prestasi')->count();
        $totalprogram = DB::table('program')->count();
        $totalberita = DB::table('berita')->count();
        $totalorganisasi = DB::table('organisasi')->count();
        $totaleskul = DB::table('eskul')->count();
        $totalpengumuman = DB::table('pengumuman')->count();
        $totalfasilitas = DB::table('fasilitas')->count();
        $totaltentang = DB::table('tentang')->count();
        $totalvisimisi = DB::table('visimisi')->count();
        $totalucapankepsek = DB::table('ucapankepsek')->count();
        $totalsejarah = DB::table('sejarah')->count();
        $totalgaleri = DB::table('galeri')->count();
        $totalslider = DB::table('slider')->count();
        $totalprofil = DB::table('profil')->count();
        $totalkalender = DB::table('kalender')->count();
        $totaluser = DB::table('users')->count();

        // Pass all variables in a single array
        return view('layouts.dashboard', ['profil' => $profil], [
            'totalguru' => $totalguru,
            'totalprestasi' => $totalprestasi,
            'totalprogram' => $totalprogram,
            'totalberita' => $totalberita,
            'totalorganisasi' => $totalorganisasi,
            'totaleskul' => $totaleskul,
            'totalpengumuman' => $totalpengumuman,
            'totalfasilitas' => $totalfasilitas,
            'totaltentang' => $totaltentang,
            'totalvisimisi' => $totalvisimisi,
            'totalucapankepsek' => $totalucapankepsek,
            'totalsejarah' => $totalsejarah,
            'totalgaleri' => $totalgaleri,
            'totalslider' => $totalslider,
            'totalprofil' => $totalprofil,
            'totalkalender' => $totalkalender,
            'totaluser' => $totaluser,
        ] );
    }
}

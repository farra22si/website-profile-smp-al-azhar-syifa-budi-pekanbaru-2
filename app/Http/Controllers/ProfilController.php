<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class ProfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::select(DB::raw("select * from profil"));
        return view('profil.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('profil.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_sekolah' => 'required',
            'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'logo_header' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        //upload image
            $logo = $request->file('logo');
            $logo2 = $request->file('logo_header');
            $logo->storeAs('public/profil', $logo->hashName());
            $logo2->storeAs('public/profil', $logo2->hashName());

            DB::insert("INSERT INTO `profil` (`id_profil`, `nama_sekolah`, `logo`, `logo_header`) VALUES (uuid(), ?, ?, ?)",
            [$request->nama_sekolah,$logo->hashName(),$logo2->hashName()]);
            return redirect()->route('profil.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('profil')->where('id_profil', $id)->first();
        return view('profil.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_sekolah' => 'required',
            'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'logo_header' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);


        //cek update file
        if ($request->file('logo', 'logo_header')) {


            $logo = $request->file('logo');
            $logo2 = $request->file('logo_header');
            $logo->storeAs('public/profil', $logo->hashName());
            $logo2->storeAs('public/profil', $logo2->hashName());


            DB::update(
                "UPDATE `profil` SET `nama_sekolah`=?,`logo`=?,`logo_header`=?  WHERE id_profil=?",
                [$request->nama_sekolah,$logo->hashName(),$logo2->hashName(), $id]
            );
        } else {
            DB::update(
                "UPDATE `profil` SET `nama_sekolah`=? WHERE id_profil=?",
                [$request->nama_sekolah, $id]
            );
        }
        return redirect()->route('profil.index')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('profil')->where('id_profil', $id)->delete();
        //redirect to index
        return redirect()->route('profil.index')->with(['success' => 'Data Berhasil Dihapus!']);
    }
}

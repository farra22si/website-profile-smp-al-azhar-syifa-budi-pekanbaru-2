<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class GaleriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::select(DB::raw("select * from galeri"));
        return view('galeri.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('galeri.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_galeri' =>'required',
            'keterangan_galeri' => 'required',
            'tanggal_galeri' => 'required',
            'file_galeri' => 'required|image|mimes:jpeg,png,jpg,gif,svg'
        ]);

        //upload image
            $file_galeri = $request->file('file_galeri');
            $file_galeri->storeAs('public/galeri', $file_galeri->hashName());


            DB::insert("INSERT INTO `galeri` (`id_galeri`, `nama_galeri`, `keterangan_galeri`, `tanggal_galeri`, `file_galeri`) VALUES (uuid(), ?, ?, ?, ?)",
            [$request->nama_galeri,$request->keterangan_galeri,$request->tanggal_galeri,$file_galeri->hashName()]);
            return redirect()->route('galeri.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('galeri')->where('id_galeri', $id)->first();
        return view('galeri.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_galeri' => 'required',
            'tanggal_galeri' => 'required',
            'keterangan_galeri' => 'required',
            'file_galeri' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ]);


        //cek update file
        if ($request->file('file_galeri')) {


            $file_galeri = $request->file('file_galeri');
            $file_galeri->storeAs('public/galeri', $file_galeri->hashName());


            DB::update(
                "UPDATE `galeri` SET `nama_galeri`=?,`keterangan_galeri`=?,`tanggal_galeri`=?,`file_galeri`=? WHERE id_galeri=?",
                [ $request->nama_galeri, $request->keterangan_galeri, $request->tanggal_galeri, $file_galeri->hashName(), $id]
            );
        } else {
            DB::update(
                "UPDATE `galeri` SET `nama_galeri`=?,`keterangan_galeri`=?,`tanggal_galeri`=? WHERE id_galeri=?",
                [$request->nama_galeri, $request->keterangan_galeri, $request->tanggal_galeri, $id]
            );
        }
        return redirect()->route('galeri.index')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('galeri')->where('id_galeri', $id)->delete();
        //redirect to index
        return redirect()->route('galeri.index')->with(['success' => 'Data Berhasil Dihapus!']);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::select(DB::raw("select * from slider"));
        return view('slider.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'keterangan' => 'required',
            'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        //upload image
            $file = $request->file('file');
            $file->storeAs('public/slider', $file->hashName());


            DB::insert("INSERT INTO `slider` (`id_slider`, `keterangan`, `file`) VALUES (uuid(), ?, ?)",
            [$request->keterangan,$file->hashName()]);
            return redirect()->route('slider.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('slider')->where('id_slider', $id)->first();
        return view('slider.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'keterangan' => 'required',
            'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);


        //cek update file
        if ($request->file('file')) {


            $file = $request->file('file');
            $file->storeAs('public/slider', $file->hashName());


            DB::update(
                "UPDATE `slider` SET `keterangan`=?,`file`=?  WHERE id_slider=?",
                [$request->keterangan,$file->hashName(), $id]
            );
        } else {
            DB::update(
                "UPDATE `slider` SET `keterangan`=? WHERE id_slider=?",
                [$request->keterangan, $id]
            );
        }
        return redirect()->route('slider.index')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('slider')->where('id_slider', $id)->delete();
        //redirect to index
        return redirect()->route('slider.index')->with(['success' => 'Data Berhasil Dihapus!']);
    }
}

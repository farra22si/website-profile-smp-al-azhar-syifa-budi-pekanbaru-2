<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class BeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::select(DB::raw("select * from berita"));
        return view('berita.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('berita.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'foto_berita' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'nama_berita' => 'required',
            'tanggal_berita' => 'required',
            'keterangan_berita' => 'required',
        ]);

        //upload image
            $image = $request->file('foto_berita');
            $image->storeAs('public/berita', $image->hashName());


            DB::insert("INSERT INTO `berita` (`id_berita`, `nama_berita`, `keterangan_berita`, `tanggal_berita`, `foto_berita`) VALUES (uuid(), ?, ?, ?, ?)",
            [$request->nama_berita,$request->keterangan_berita,$request->tanggal_berita,$image->hashName()]);
            return redirect()->route('berita.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('berita')->where('id_berita', $id)->first();
        return view('berita.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'foto_berita' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'nama_berita' => 'required',
            'tanggal_berita' => 'required',
            'keterangan_berita' => 'required',
        ]);


        //cek update foto
        if ($request->file('foto_berita')) {


            $image = $request->file('foto_berita');
            $image->storeAs('public/berita', $image->hashName());


            DB::update(
                "UPDATE `berita` SET `nama_berita`=?,`keterangan_berita`=?,`tanggal_berita`=?,`foto_berita`=? WHERE id_berita=?",
                [$request->nama_berita, $request->keterangan_berita, $request->tanggal_berita, $image->hashName(), $id]
            );
        } else {
            DB::update(
                "UPDATE `berita` SET `nama_berita`=?,`keterangan_berita`=?,`tanggal_berita`=? WHERE id_berita=?",
                [$request->nama_berita, $request->keterangan_berita, $request->tanggal_berita, $id]
            );
        }
        return redirect()->route('berita.index')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('berita')->where('id_berita', $id)->delete();
        //redirect to index
        return redirect()->route('berita.index')->with(['success' => 'Data Berhasil Dihapus!']);
    }
}
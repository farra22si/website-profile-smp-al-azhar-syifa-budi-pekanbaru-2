<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class VisimisiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::select(DB::raw("select * from visimisi"));
        return view('visimisi.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('visimisi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'visi' => 'required',
            'misi' => 'required',
        ]);

        DB::insert(
            "INSERT INTO `visimisi` (`id_visimisi`, `visi`, `misi`) VALUES (uuid(), ?, ?)",
            [$request->visi, $request->misi]
        );
        return redirect()->route('visimisi.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('visimisi')->where('id_visimisi', $id)->first();
        return view('visimisi.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'visi' => 'required',
            'misi' => 'required',
        ]);

        DB::update(
            "UPDATE `visimisi` SET `visi`=?,`misi`=? WHERE id_visimisi=?",
            [$request->visi, $request->misi, $id]
        );
        return redirect()->route('visimisi.index')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('visimisi')->where('id_visimisi', $id)->delete();
        //redirect to index
        return redirect()->route('visimisi.index')->with(['success' => 'Data Berhasil Dihapus!']);
    }
}

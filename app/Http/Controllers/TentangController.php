<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class TentangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::select(DB::raw("select * from tentang"));
        return view('tentang.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tentang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'telepon' => 'required',
            'instagram' => 'required',
            'email' => 'required',
            'alamat' => 'required',
            'facebook' => 'required',
        ]);

        DB::insert(
            "INSERT INTO `tentang` (`id_tentang`, `telepon`, `instagram`, `email`, `alamat`, `facebook`) VALUES (uuid(), ?, ?, ?, ?, ?)",
            [$request->telepon, $request->instagram, $request->email, $request->alamat, $request->facebook]
        );
        return redirect()->route('tentang.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('tentang')->where('id_tentang', $id)->first();
        return view('tentang.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'telepon' => 'required',
            'instagram' => 'required',
            'email' => 'required',
            'alamat' => 'required',
            'facebook' => 'required',
        ]);

        DB::update(
            "UPDATE `tentang` SET `telepon`=?,`instagram`=?,`email`=?,`alamat`=?,`facebook`=? WHERE id_tentang=?",
            [$request->telepon, $request->instagram, $request->email, $request->alamat, $request->facebook, $id]
        );
        return redirect()->route('tentang.index')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('tentang')->where('id_tentang', $id)->delete();
        //redirect to index
        return redirect()->route('tentang.index')->with(['success' => 'Data Berhasil Dihapus!']);
    }
}

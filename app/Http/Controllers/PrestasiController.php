<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class PrestasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::select(DB::raw("select * from prestasi"));
        return view('prestasi.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('prestasi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_prestasi' =>'required',
            'keterangan_prestasi' => 'required',
            'tanggal_prestasi' => 'required',
            'foto_prestasi' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        //upload image
            $image = $request->file('foto_prestasi');
            $image->storeAs('public/prestasi', $image->hashName());


            DB::insert("INSERT INTO `prestasi` (`id_prestasi`, `nama_prestasi`, `keterangan_prestasi`, `tanggal_prestasi`, `foto_prestasi`) VALUES (uuid(), ?, ?, ?, ?)",
            [$request->nama_prestasi,$request->keterangan_prestasi,$request->tanggal_prestasi,$image->hashName()]);
            return redirect()->route('prestasi.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('prestasi')->where('id_prestasi', $id)->first();
        return view('prestasi.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_prestasi' => 'required',
            'tanggal_prestasi' => 'required',
            'keterangan_prestasi' => 'required',
            'foto_prestasi' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);


        //cek update foto
        if ($request->file('foto_prestasi')) {


            $image = $request->file('foto_prestasi');
            $image->storeAs('public/prestasi', $image->hashName());


            DB::update(
                "UPDATE `prestasi` SET `nama_prestasi`=?,`keterangan_prestasi`=?,`tanggal_prestasi`=?,`foto_prestasi`=? WHERE id_prestasi=?",
                [ $request->nama_prestasi, $request->keterangan_prestasi, $request->tanggal_prestasi,$image->hashName(), $id]
            );
        } else {
            DB::update(
                "UPDATE `prestasi` SET `nama_prestasi`=?,`keterangan_prestasi`=?,`tanggal_prestasi`=? WHERE id_prestasi=?",
                [$request->nama_prestasi, $request->keterangan_prestasi, $request->tanggal_prestasi, $id]
            );
        }
        return redirect()->route('prestasi.index')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('prestasi')->where('id_prestasi', $id)->delete();
        //redirect to index
        return redirect()->route('prestasi.index')->with(['success' => 'Data Berhasil Dihapus!']);
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class ProgramSekolahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::select(DB::raw("select * from program"));
        return view('program.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('program.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_program' =>'required',
            'keterangan_program' => 'required',
            'tanggal_program' => 'required',
            'foto_program' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        //upload image
            $foto_program = $request->file('foto_program');
            $foto_program->storeAs('public/program', $foto_program->hashName());


            DB::insert("INSERT INTO `program` (`id_program`, `nama_program`, `keterangan_program`, `tanggal_program`, `foto_program`) VALUES (uuid(), ?, ?, ?, ?)",
            [$request->nama_program,$request->keterangan_program,$request->tanggal_program,$foto_program->hashName()]);
            return redirect()->route('program.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('program')->where('id_program', $id)->first();
        return view('program.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_program' => 'required',
            'tanggal_program' => 'required',
            'keterangan_program' => 'required',
            'foto_program' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);


        //cek update foto
        if ($request->file('foto_program')) {


            $foto_program = $request->file('foto_program');
            $foto_program->storeAs('public/program', $foto_program->hashName());


            DB::update(
                "UPDATE `program` SET `nama_program`=?,`keterangan_program`=?,`tanggal_program`=?,`foto_program`=? WHERE id_program=?",
                [ $request->nama_program, $request->keterangan_program, $request->tanggal_program, $foto_program->hashName(), $id]
            );
        } else {
            DB::update(
                "UPDATE `program` SET `nama_program`=?,`keterangan_program`=?,`tanggal_program`=? WHERE id_program=?",
                [$request->nama_program, $request->keterangan_program, $request->tanggal_program, $id]
            );
        }
        return redirect()->route('program.index')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('program')->where('id_program', $id)->delete();
        //redirect to index
        return redirect()->route('program.index')->with(['success' => 'Data Berhasil Dihapus!']);
    }
}

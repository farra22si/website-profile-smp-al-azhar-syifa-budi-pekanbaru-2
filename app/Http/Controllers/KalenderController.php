<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class KalenderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::select(DB::raw("select * from kalender"));
        return view('kalender.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kalender.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'tanggal' =>'required',
            'keterangan' => 'required',
            'file_1' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'file_2' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        //upload image
        $file_1 = $request->file('file_1');
        $file_2 = $request->file('file_2');
        $file_1->storeAs('public/kalender', $file_1->hashName());
        $file_2->storeAs('public/kalender', $file_2->hashName());


            DB::insert("INSERT INTO `kalender` (`id_kalender`, `tanggal`, `keterangan`, `file_1`, `file_2`) VALUES (uuid(), ?, ?, ?, ?)",
            [$request->tanggal,$request->keterangan,$file_1->hashName(),$file_2->hashName()]);
            return redirect()->route('kalender.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('kalender')->where('id_kalender', $id)->first();
        return view('kalender.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'tanggal' => 'required',
            'keterangan' => 'required',
            'file_1' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'file_2' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);


        //cek update file
        if ($request->file('file_1', 'file_2')) {


            $file_1 = $request->file('file_1');
            $file_2 = $request->file('file_2');
            $file_1->storeAs('public/kalender', $file_1->hashName());
            $file_2->storeAs('public/kalender', $file_2->hashName());


            DB::update(
                "UPDATE `kalender` SET `tanggal`=?,`keterangan`=?,`file_1`=?,`file_2`=?  WHERE id_kalender=?",
                [ $request->tanggal, $request->keterangan,$file_1->hashName(),$file_2->hashName(), $id]
            );
        } else {
            DB::update(
                "UPDATE `kalender` SET `tanggal`=?,`keterangan`=? WHERE id_kalender=?",
                [$request->tanggal, $request->keterangan, $id]
            );
        }
        return redirect()->route('kalender.index')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('kalender')->where('id_kalender', $id)->delete();
        //redirect to index
        return redirect()->route('kalender.index')->with(['success' => 'Data Berhasil Dihapus!']);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class PengumumanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::select(DB::raw("select * from pengumuman"));
        return view('pengumuman.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pengumuman.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'foto_pengumuman' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'nama_pengumuman' => 'required',
            'tanggal_pengumuman' => 'required',
            'keterangan_pengumuman' => 'required',
        ]);

        //upload image
            $image = $request->file('foto_pengumuman');
            $image->storeAs('public/pengumuman', $image->hashName());


            DB::insert("INSERT INTO `pengumuman` (`id_pengumuman`, `nama_pengumuman`, `keterangan_pengumuman`, `tanggal_pengumuman`, `foto_pengumuman`) VALUES (uuid(), ?, ?, ?, ?)",
            [$request->nama_pengumuman,$request->keterangan_pengumuman,$request->tanggal_pengumuman,$image->hashName()]);
            return redirect()->route('pengumuman.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('pengumuman')->where('id_pengumuman', $id)->first();
        return view('pengumuman.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'foto_pengumuman' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'nama_pengumuman' => 'required',
            'tanggal_pengumuman' => 'required',
            'keterangan_pengumuman' => 'required',
        ]);


        //cek update foto
        if ($request->file('foto_pengumuman')) {


            $image = $request->file('foto_pengumuman');
            $image->storeAs('public/pengumuman', $image->hashName());


            DB::update(
                "UPDATE `pengumuman` SET `nama_pengumuman`=?,`keterangan_pengumuman`=?,`tanggal_pengumuman`=?,`foto_pengumuman`=? WHERE id_pengumuman=?",
                [$request->nama_pengumuman, $request->keterangan_pengumuman, $request->tanggal_pengumuman, $image->hashName(), $id]
            );
        } else {
            DB::update(
                "UPDATE `pengumuman` SET `nama_pengumuman`=?,`keterangan_pengumuman`=?,`tanggal_pengumuman`=? WHERE id_pengumuman=?",
                [$request->nama_pengumuman, $request->keterangan_pengumuman, $request->tanggal_pengumuman, $id]
            );
        }
        return redirect()->route('pengumuman.index')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('pengumuman')->where('id_pengumuman', $id)->delete();
        //redirect to index
        return redirect()->route('pengumuman.index')->with(['success' => 'Data Berhasil Dihapus!']);
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class UcapanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::select(DB::raw("select * from ucapankepsek"));
        return view('ucapankepsek.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ucapankepsek.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' =>'required',
            'keterangan' => 'required',
            'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        //upload image
            $foto = $request->file('foto');
            $foto->storeAs('public/ucapankepsek', $foto->hashName());


            DB::insert("INSERT INTO `ucapankepsek` (`id_ucapan`, `nama`, `keterangan`, `foto`) VALUES (uuid(), ?, ?, ?)",
            [$request->nama,$request->keterangan,$foto->hashName()]);
            return redirect()->route('ucapankepsek.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('ucapankepsek')->where('id_ucapan', $id)->first();
        return view('ucapankepsek.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama' => 'required',
            'keterangan' => 'required',
            'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);


        //cek update foto
        if ($request->file('foto')) {


            $foto = $request->file('foto');
            $foto->storeAs('public/ucapankepsek', $foto->hashName());


            DB::update(
                "UPDATE `ucapankepsek` SET `nama`=?,`keterangan`=?,`foto`=?  WHERE id_ucapan=?",
                [ $request->nama, $request->keterangan,$foto->hashName(), $id]
            );
        } else {
            DB::update(
                "UPDATE `ucapankepsek` SET `nama`=?,`keterangan`=? WHERE id_ucapan=?",
                [$request->nama, $request->keterangan, $id]
            );
        }
        return redirect()->route('ucapankepsek.index')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('ucapankepsek')->where('id_ucapan', $id)->delete();
        //redirect to index
        return redirect()->route('ucapankepsek.index')->with(['success' => 'Data Berhasil Dihapus!']);
    }
}

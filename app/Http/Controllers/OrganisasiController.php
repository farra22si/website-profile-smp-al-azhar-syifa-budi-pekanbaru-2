<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class organisasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::select(DB::raw("select * from organisasi"));
        return view('organisasi.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('organisasi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'foto_organisasi' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'nama_organisasi' => 'required',
            'tanggal_organisasi' => 'required',
            'keterangan_organisasi' => 'required',
        ]);

        //upload image
            $image = $request->file('foto_organisasi');
            $image->storeAs('public/organisasi', $image->hashName());


            DB::insert("INSERT INTO `organisasi` (`id_organisasi`, `nama_organisasi`, `keterangan_organisasi`, `tanggal_organisasi`, `foto_organisasi`) VALUES (uuid(), ?, ?, ?, ?)",
            [$request->nama_organisasi,$request->keterangan_organisasi,$request->tanggal_organisasi,$image->hashName()]);
            return redirect()->route('organisasi.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('organisasi')->where('id_organisasi', $id)->first();
        return view('organisasi.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'foto_organisasi' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'nama_organisasi' => 'required',
            'tanggal_organisasi' => 'required',
            'keterangan_organisasi' => 'required',
        ]);


        //cek update foto
        if ($request->file('foto_organisasi')) {


            $image = $request->file('foto_organisasi');
            $image->storeAs('public/organisasi', $image->hashName());


            DB::update(
                "UPDATE `organisasi` SET `nama_organisasi`=?,`keterangan_organisasi`=?,`tanggal_organisasi`=?,`foto_organisasi`=? WHERE id_organisasi=?",
                [$request->nama_organisasi, $request->keterangan_organisasi, $request->tanggal_organisasi, $image->hashName(), $id]
            );
        } else {
            DB::update(
                "UPDATE `organisasi` SET `nama_organisasi`=?,`keterangan_organisasi`=?,`tanggal_organisasi`=? WHERE id_organisasi=?",
                [$request->nama_organisasi, $request->keterangan_organisasi, $request->tanggal_organisasi, $id]
            );
        }
        return redirect()->route('organisasi.index')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('organisasi')->where('id_organisasi', $id)->delete();
        //redirect to index
        return redirect()->route('organisasi.index')->with(['success' => 'Data Berhasil Dihapus!']);
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    public function index()
    {
        $profil = DB::select(DB::raw("select * from profil"));
        $ucapan = DB::select(DB::raw("select * from ucapankepsek"));
        $sejarah = DB::select(DB::raw("select * from sejarah"));
        $berita = DB::table('berita')->orderBy('tanggal_berita', 'desc')->paginate(3);
        $program = DB::table('program')->orderBy('tanggal_program', 'desc')->paginate(6);
        $data = DB::select(DB::raw("select * from slider"));
        $tentang = DB::select(DB::raw("select * from tentang"));
        return view('home.index', compact('data', 'tentang', 'berita', 'program', 'ucapan', 'sejarah', 'profil'));
    }
    public function ucapan()
    {
        $profil = DB::select(DB::raw("select * from profil"));
        $ucapan = DB::select(DB::raw("select * from ucapankepsek"));
        $galeri = DB::select(DB::raw("select * from galeri"));
        $tentang = DB::select(DB::raw("select * from tentang"));
        return view('home.ucapan', compact('tentang', 'galeri', 'ucapan', 'profil'));
    }
    public function guru()
    {
        $profil = DB::select(DB::raw("select * from profil"));
        $galeri = DB::select(DB::raw("select * from galeri"));
        $data = DB::select(DB::raw("select * from guru"));
        $tentang = DB::select(DB::raw("select * from tentang"));
        return view('home.guru', compact('data', 'galeri', 'tentang', 'profil'));
    }
    public function prestasi()
    {
        $galeri = DB::select(DB::raw("select * from galeri"));
        $data = DB::select(DB::raw("SELECT * FROM prestasi ORDER BY tanggal_prestasi DESC"));
        $tentang = DB::select(DB::raw("select * from tentang"));
        return view('home.prestasi', compact('data', 'tentang', 'galeri'));
    }
    public function fasilitas()
    {
        $galeri = DB::select(DB::raw("select * from galeri"));
        $data = DB::select(DB::raw("select * from fasilitas"));
        $tentang = DB::select(DB::raw("select * from tentang"));
        return view('home.fasilitas', compact('data', 'tentang', 'galeri'));
    }
    public function galeri()
    {
        $data = DB::select(DB::raw("SELECT * FROM galeri ORDER BY tanggal_galeri DESC"));
        $tentang = DB::select(DB::raw("select * from tentang"));
        return view('home.galeri', compact('data', 'tentang'));
    }
    public function visimisi()
    {
        $visimisi = DB::select(DB::raw("select * from visimisi"));
        $galeri = DB::select(DB::raw("select * from galeri"));
        $tentang = DB::select(DB::raw("select * from tentang"));
        return view('home.visimisi', compact('tentang', 'galeri', 'visimisi'));
    }
    public function sejarah()
    {
        $galeri = DB::select(DB::raw("select * from galeri"));
        $tentang = DB::select(DB::raw("select * from tentang"));
        return view('home.sejarah', compact('tentang', 'galeri'));
    }
    public function program()
    {
        $galeri = DB::select(DB::raw("select * from galeri"));
        $data = DB::select(DB::raw("SELECT * FROM program ORDER BY tanggal_program DESC"));
        $tentang = DB::select(DB::raw("select * from tentang"));
        return view('home.program', compact('data', 'galeri', 'tentang'));
    }
    public function kalender()
    {
        $kalender = DB::select(DB::raw("select * from kalender"));
        $galeri = DB::select(DB::raw("select * from galeri"));
        $tentang = DB::select(DB::raw("select * from tentang"));
        return view('home.kalender', compact('tentang', 'galeri', 'kalender'));
    }
    public function pengumuman()
    {
        $galeri = DB::select(DB::raw("select * from galeri"));
        $data = DB::select(DB::raw("select * from pengumuman"));
        $tentang = DB::select(DB::raw("select * from tentang"));
        return view('home.pengumuman', compact('data', 'tentang', 'galeri'));
    }
    public function berita()
    {
        $data = DB::table('berita')->orderBy('tanggal_berita', 'desc')->paginate(6);
        $tentang = DB::select(DB::raw("select * from tentang"));
        $galeri = DB::select(DB::raw("select * from galeri"));
        return view('home.berita', compact('data', 'tentang', 'galeri'));
    }
    public function isiberita($id)
    {
        $berita = DB::table('berita')->where('id_berita', $id)->first();
        $tentang = DB::select(DB::raw("select * from tentang"));
        $galeri = DB::select(DB::raw("select * from galeri"));
        $recentPosts = DB::table('berita')->orderBy('tanggal_berita', 'desc')->limit(7)->get();
        return view('home.isiberita', compact('berita', 'tentang', 'galeri', 'recentPosts'));
    }
    public function organisasi()
    {
        $organisasi = DB::select(DB::raw("select * from organisasi"));
        $tentang = DB::select(DB::raw("select * from tentang"));
        $galeri = DB::select(DB::raw("select * from galeri"));
        return view('home.organisasi', compact('organisasi', 'tentang', 'galeri'));
    }
    public function eskul()
    {
        $galeri = DB::select(DB::raw("select * from galeri"));
        $eskul = DB::select(DB::raw("select * from eskul"));
        $tentang = DB::select(DB::raw("select * from tentang"));
        return view('home.eskul', compact('eskul', 'tentang', 'galeri'));
    }
    public function kontak()
    {
        $galeri = DB::select(DB::raw("select * from galeri"));
        $tentang = DB::select(DB::raw("select * from tentang"));
        return view('home.kontak', compact('tentang', 'galeri'));
    }
    public function detaileskul($id)
    {
        $eskul = DB::table('eskul')->where('id_eskul', $id)->first();
        $tentang = DB::select(DB::raw("select * from tentang"));
        $galeri = DB::select(DB::raw("select * from galeri"));
        $recentPosts = DB::table('eskul')->orderBy('nama_eskul', 'desc')->limit(5)->get();
        return view('home.detaileskul', compact('eskul', 'tentang', 'galeri', 'recentPosts'));
    }
}

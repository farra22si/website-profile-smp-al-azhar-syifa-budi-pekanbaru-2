<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class FasilitasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::select(DB::raw("select * from fasilitas"));
        return view('fasilitas.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fasilitas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            
            'nama_fasilitas' => 'required',
            'tanggal_fasilitas' => 'required',
            'keterangan_fasilitas' => 'required',
            'foto_fasilitas' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        //upload image
        $foto_fasilitas = $request->file('foto_fasilitas');
            $foto_fasilitas->storeAs('public/fasilitas', $foto_fasilitas->hashName());

            DB::insert("INSERT INTO `fasilitas` (`id_fasilitas`, `nama_fasilitas`, `keterangan_fasilitas`, `tanggal_fasilitas`, `foto_fasilitas`) VALUES (uuid(), ?, ?, ?, ?)",
            [$request->nama_fasilitas,$request->keterangan_fasilitas,$request->tanggal_fasilitas,$foto_fasilitas->hashName()]);
            return redirect()->route('fasilitas.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('fasilitas')->where('id_fasilitas', $id)->first();
        return view('fasilitas.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'foto_fasilitas' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'nama_fasilitas' => 'required',
            'tanggal_fasilitas' => 'required',
            'keterangan_fasilitas' => 'required',
        ]);


        //cek update foto
        if ($request->file('foto_fasilitas')) {


            $image = $request->file('foto_fasilitas');
            $image->storeAs('public/fasilitas', $image->hashName());


            DB::update(
                "UPDATE `fasilitas` SET `nama_fasilitas`=?,`keterangan_fasilitas`=?,`tanggal_fasilitas`=?,`foto_fasilitas`=? WHERE id_fasilitas=?",
                [$request->nama_fasilitas, $request->keterangan_fasilitas, $request->tanggal_fasilitas, $image->hashName(), $id]
            );
        } else {
            DB::update(
                "UPDATE `fasilitas` SET `nama_fasilitas`=?,`keterangan_fasilitas`=?,`tanggal_fasilitas`=? WHERE id_fasilitas=?",
                [$request->nama_fasilitas, $request->keterangan_fasilitas, $request->tanggal_fasilitas, $id]
            );
        }
        return redirect()->route('fasilitas.index')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('fasilitas')->where('id_fasilitas', $id)->delete();
        //redirect to index
        return redirect()->route('fasilitas.index')->with(['success' => 'Data Berhasil Dihapus!']);
    }
}
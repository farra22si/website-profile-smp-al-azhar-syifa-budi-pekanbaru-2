<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class eskulController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::select(DB::raw("select * from eskul"));
        return view('eskul.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('eskul.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'foto_eskul' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'nama_eskul' => 'required',
            'tanggal_eskul' => 'required',
            'keterangan_eskul' => 'required',
        ]);

        //upload image
            $image = $request->file('foto_eskul');
            $image->storeAs('public/eskul', $image->hashName());


            DB::insert("INSERT INTO `eskul` (`id_eskul`, `nama_eskul`, `keterangan_eskul`, `tanggal_eskul`, `foto_eskul`) VALUES (uuid(), ?, ?, ?, ?)",
            [$request->nama_eskul,$request->keterangan_eskul,$request->tanggal_eskul,$image->hashName()]);
            return redirect()->route('eskul.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('eskul')->where('id_eskul', $id)->first();
        return view('eskul.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'foto_eskul' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'nama_eskul' => 'required',
            'tanggal_eskul' => 'required',
            'keterangan_eskul' => 'required',
        ]);


        //cek update foto
        if ($request->file('foto_eskul')) {


            $image = $request->file('foto_eskul');
            $image->storeAs('public/eskul', $image->hashName());


            DB::update(
                "UPDATE `eskul` SET `nama_eskul`=?,`keterangan_eskul`=?,`tanggal_eskul`=?,`foto_eskul`=? WHERE id_eskul=?",
                [$request->nama_eskul, $request->keterangan_eskul, $request->tanggal_eskul, $image->hashName(), $id]
            );
        } else {
            DB::update(
                "UPDATE `eskul` SET `nama_eskul`=?,`keterangan_eskul`=?,`tanggal_eskul`=? WHERE id_eskul=?",
                [$request->nama_eskul, $request->keterangan_eskul, $request->tanggal_eskul, $id]
            );
        }
        return redirect()->route('eskul.index')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('eskul')->where('id_eskul', $id)->delete();
        //redirect to index
        return redirect()->route('eskul.index')->with(['success' => 'Data Berhasil Dihapus!']);
    }
}
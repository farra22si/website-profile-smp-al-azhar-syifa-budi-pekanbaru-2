@extends('layouts.app-master')
@section('content')
@auth
<main id="main" class="main">

    <div class="pagetitle">
        <h1> Informasi </h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('layouts.dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item">Tables</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Informasi</h5>
                        <a href="{{ route('berita.create') }}" class="btn btn-md btn-success mb-3">TAMBAH DATA INFORMASI</a>


                        <!-- Table with stripped rows -->
                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>NAMA</th>
                                    <th>KETERANGAN</th>
                                    <th>TANGGAL</th>
                                    <th>FOTO</th>
                                    <th>AKSI</th>

                                </tr>
                            </thead>
                            <tbody>
                                @php($no = 1)
                                @forelse ($data as $berita)
                                <tr class="text-center">
                                    <td>{{ $no ++ }}</td>

                                    <td>{{ $berita->nama_berita}}</td>

                                    <td>{!! $berita->keterangan_berita !!}</td>

                                    <td>{{ $berita->tanggal_berita}}</td>

                                    <td class="text-center">
                                        <img src="{{Storage::url('public/berita/') . $berita->foto_berita }}" class="rounded" style="width: 150px">

                                    </td>

                                    <td class="text-center">
                                        <form onsubmit="return confirm('Apakah Anda Yakin ?');" action="{{route('berita.destroy', $berita->id_berita) }}" method="post">
                                            <a href="{{route('berita.edit', $berita->id_berita) }}" class="btn btn-sm btn-primary">EDIT</a>

                                            @csrf
                                            @method('DELETE')

                                            <button type="submit" class="btn btn-sm btn-danger">HAPUS</button>
                                        </form>
                                    </td>
                                </tr>
                                @empty
                                <div class="alert alert-danger">
                                    Data Berita belum

                                    Tersedia.

                                </div>
                                @endforelse
                            </tbody>
                        </table>
                        <!-- End Table with stripped rows -->

                    </div>
                </div>

            </div>
        </div>
    </section>
    <script>
        //message with toastr
        @if(session() -> has('success'))
        toastr.success('{{ session('
            success ') }}', 'BERHASIL!');
        @elseif(session() -> has('error'))
        toastr.error('{{ session('
            error ') }}', 'GAGAL!');
        @endif
    </script>
</main><!-- End #main -->

@endauth
@endsection
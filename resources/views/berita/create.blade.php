@extends('layouts.app-master')
@section('content')
@auth
<main id="main" class="main">

    <div class="pagetitle">
        <h1> Informasi</h1>
        <nav>
        <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('layouts.dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('berita.index') }}">Tables</a></li>
                <li class="breadcrumb-item active">Tambah Data</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Tambah Informasi </h5>
                        <form action="{{ route('berita.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="col-12">
                                <label class="form-label">Judul Berita </label>
                                <input type="text" class="form-control @error('nama_berita') is-invalid @enderror" name="nama_berita" placeholder="Masukkan Judul Berita">
                                <!-- error message untuk nama_berita -->
                                @error('nama_berita')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Keterangan</label>
                                <textarea class="form-control @error('keterangan_berita') is-invalid @enderror" rows="5" placeholder="Masukkan Keterangan Berita" name="keterangan_berita">{{ old('keterangan_berita') }}</textarea>
                                <!-- error message untuk nama_berita -->
                                @error('keterangan_berita')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Tanggal</label>
                                <input type="date" class="form-control @error('tanggal_berita') is-invalid @enderror" name="tanggal_berita">
                                <!-- error message untuk tanggal_berita -->
                                @error('tanggal_berita')
                                <div class=" alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Foto Berita </label>
                                <input type="file" class="form-control @error('foto_berita') is-invalid @enderror" name="foto_berita">
                                <!-- error message untuk title -->
                                @error('foto_berita')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div> <br>
                            <div class="text-center">
                                <button type="submit" class="btn btn-md btn-primary">SIMPAN</button>
                                <button type="reset" class="btn btn-md btn-warning">RESET</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
        <script>
            CKEDITOR.replace('keterangan_berita');
        </script>
    </section>
</main><!-- End #main -->
@endauth
@endsection
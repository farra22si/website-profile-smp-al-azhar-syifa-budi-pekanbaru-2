@extends('layouts.app-master')
@section('content')
@auth
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Organisasi</h1>
        <nav>
        <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('layouts.dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('organisasi.index') }}">Tables</a></li>
                <li class="breadcrumb-item active">Tambah Data</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Tambah Organisasi </h5>
                        <form action="{{ route('organisasi.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="col-12">
                                <label class="form-label">Judul Organisasi </label>
                                <input type="text" class="form-control @error('nama_organisasi') is-invalid @enderror" name="nama_organisasi" placeholder="Masukkan Judul Organisasi">
                                <!-- error message untuk nama_organisasi -->
                                @error('nama_organisasi')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Keterangan</label>
                                <textarea class="form-control @error('keterangan_organisasi') is-invalid @enderror" rows="5" placeholder="Masukkan Keterangan" name="keterangan_organisasi">{{ old('keterangan_organisasi') }}</textarea>
                                <!-- error message untuk nama_organisasi -->
                                @error('keterangan_organisasi')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Tanggal</label>
                                <input type="date" class="form-control @error('tanggal_organisasi') is-invalid @enderror" name="tanggal_organisasi">
                                <!-- error message untuk tanggal_organisasi -->
                                @error('tanggal_organisasi')
                                <div class=" alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Foto Organisasi </label>
                                <input type="file" class="form-control @error('foto_organisasi') is-invalid @enderror" name="foto_organisasi">
                                <!-- error message untuk title -->
                                @error('foto_organisasi')
                                <div class="alert alert-danger mt-2">

                                    {{ $message }}
                                </div>
                                @enderror
                            </div> <br>
                            <div class="text-center">
                                <button type="submit" class="btn btn-md btn-primary">SIMPAN</button>
                                <button type="reset" class="btn btn-md btn-warning">RESET</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
        <script>
            CKEDITOR.replace('keterangan_organisasi');
        </script>
    </section>
</main><!-- End #main -->
@endauth
@endsection
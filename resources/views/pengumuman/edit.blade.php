@extends('layouts.app-master')
@section('content')
@auth
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Pengumuman</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item">Tables</li>
                <li class="breadcrumb-item active">Data</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Edit Pengumuman</h5>
                        <form action="{{ route('pengumuman.update', $data->id_pengumuman) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="col-12">
                                <label class="form-label">Judul Pengumuman</label>
                                <input type="text" class="form-control @error('nama_pengumuman') is-invalid @enderror" name="nama_pengumuman" placeholder="Masukkan Judul Pengumuman" value="{{ $data->nama_pengumuman }}">
                                <!-- error message untuk nama_pengumuman -->
                                @error('nama_pengumuman')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Keterangan</label>
                                <div class="quill-editor-full">
                                    <textarea class="form-control @error('keterangan_pengumuman') is-invalid @enderror" placeholder="Masukkan Keterangan" name="keterangan_pengumuman" style="height: 100px;">{{ $data->keterangan_pengumuman }}</textarea>
                                    <!-- error message untuk nama_pengumuman -->
                                    @error('keterangan_pengumuman')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Tanggal</label>
                                <input type="date" class="form-control @error('tanggal_pengumuman') is-invalid @enderror" name="tanggal_pengumuman" value="{{ $data->tanggal_pengumuman }}">
                                <!-- error message untuk tanggal_pengumuman -->
                                @error('tanggal_pengumuman')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Foto Pengumuman</label>
                                <input type="file" class="form-control @error('foto_pengumuman') is-invalid @enderror" name="foto_pengumuman">
                                <!-- error message untuk title -->
                                @error('foto_pengumuman')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div> <br>
                            <div class="text-center">
                                <button type="submit" class="btn btn-md btn-primary">SIMPAN</button>
                                <button type="reset" class="btn btn-md btn-warning">RESET</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script>
            CKEDITOR.replace('keterangan_pengumuman');
        </script>
    </section>
</main><!-- End #main -->
@endauth
@endsection
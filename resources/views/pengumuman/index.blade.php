@extends('layouts.app-master')

@section('content')
@auth
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Data Pengumuman</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item">Tables</li>
                <li class="breadcrumb-item active">Data</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Pengumuman</h5>
                        <a href="{{ route('pengumuman.create') }}" class="btn btn-md btn-success mb-3">TAMBAH DATA PENGUMUMAN</a>


                        <!-- Table with stripped rows -->
                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>NAMA</th>
                                    <th>KETERANGAN</th>
                                    <th>TANGGAL</th>
                                    <th>FOTO</th>
                                    <th>AKSI</th>

                                </tr>
                            </thead>
                            <tbody>
                            @php($no = 1)
                                @forelse ($data as $pengumuman)
                                <tr class="text-center">
                                    <td>{{ $no ++ }}</td>

                                    <td>{{ $pengumuman->nama_pengumuman}}</td>

                                    <td>{!! $pengumuman->keterangan_pengumuman !!}</td>

                                    <td>{{ $pengumuman->tanggal_pengumuman}}</td>

                                    <td class="text-center">
                                        <img src="{{Storage::url('public/pengumuman/') . $pengumuman->foto_pengumuman }}" class="rounded" style="width: 150px">

                                    </td>

                                    <td class="text-center">
                                        <form onsubmit="return confirm('Apakah Anda Yakin ?');" action="{{route('pengumuman.destroy', $pengumuman->id_pengumuman) }}" method="post">
                                            <a href="{{route('pengumuman.edit', $pengumuman->id_pengumuman) }}" class="btn btn-sm btn-primary">EDIT</a>

                                            @csrf
                                            @method('DELETE')

                                            <button type="submit" class="btn btn-sm btn-danger">HAPUS</button>
                                        </form>
                                    </td>
                                </tr>
                                @empty
                                <div class="alert alert-danger">
                                    Data Pengumuman Belum Tersedia.

                                </div>
                                @endforelse
                            </tbody>
                        </table>
                        <!-- End Table with stripped rows -->

                    </div>
                </div>

            </div>
        </div>
    </section>
    <script>
        //message with toastr
        @if(session() -> has('success'))
        toastr.success('{{ session('
            success ') }}', 'BERHASIL!');
        @elseif(session() -> has('error'))
        toastr.error('{{ session('
            error ') }}', 'GAGAL!');
        @endif
    </script>
</main><!-- End #main -->

@endauth
@endsection
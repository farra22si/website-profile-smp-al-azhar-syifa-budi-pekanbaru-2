@extends('layouts.app-master')
@section('content')
@auth
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Ucapan Kepala Sekolah</h1>
        <nav>
        <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('layouts.dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('ucapankepsek.index') }}">Tables</a></li>
                <li class="breadcrumb-item active">Tambah Data</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Tambah Ucapan Kepala Sekolah</h5>
                        <form action="{{ route('ucapankepsek.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="col-12">
                                <label class="form-label">Nama Kepala Sekolah</label>
                                <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" placeholder="Masukkan Nama">
                                <!-- error message untuk nama -->
                                @error('nama')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Keterangan</label>
                                <textarea class="form-control @error('keterangan') is-invalid @enderror" rows="5" placeholder="Masukkan keterangan" name="keterangan">{{ old('keterangan') }}</textarea>
                                <!-- error message untuk nama_program -->
                                @error('keterangan')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Foto</label>
                                <input type="file" class="form-control @error('foto') is-invalid @enderror" name="foto">
                                <!-- error message untuk title -->
                                @error('foto')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="text-center">
                                <button type="submit" class="btn btn-md btn-primary">SIMPAN</button>
                                <button type="reset" class="btn btn-md btn-warning">RESET</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
        <script>
            CKEDITOR.replace('keterangan');
        </script>
    </section>
</main><!-- End #main -->

@endauth
@endsection
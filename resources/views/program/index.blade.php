@extends('layouts.app-master')

@section('content')
@auth
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Program Sekolah</h1>
        <nav>
        <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('layouts.dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item">Tables</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Program Sekolah</h5>
                        <a href="{{ route('program.create') }}" class="btn btn-md btn-success mb-3">TAMBAH DATA PROGRAM SEKOLAH</a>
                        <!-- Table with stripped rows -->
                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>JUDUL</th>
                                    <th>KETERANGAN</th>
                                    <th>TANGGAL</th>
                                    <th>FOTO</th>
                                    <th>AKSI</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php($no = 1)
                                @forelse ($data as $program)
                                <tr class="text-center">
                                    <td>{{ $no ++ }}</td>

                                    <td>{{ $program->nama_program}}</td>

                                    <td>{!! $program->keterangan_program !!}</td>

                                    <td>{{ $program->tanggal_program}}</td>

                                    <td class="text-center">
                                        <img src="{{Storage::url('public/program/') . $program->foto_program }}" class="rounded" style="width: 150px">

                                    </td>
                                    <td class="text-center">
                                        <form onsubmit="return confirm('Apakah Anda Yakin ?');" action="{{route('program.destroy', $program->id_program) }}" method="post">
                                            <a href="{{route('program.edit', $program->id_program) }}" class="btn btn-sm btn-primary">EDIT</a>

                                            @csrf
                                            @method('DELETE')

                                            <button type="submit" class="btn btn-sm btn-danger">HAPUS</button>
                                        </form>
                                    </td>
                                </tr>
                                @empty
                                <div class="alert alert-danger">
                                    Data Program Belum Tersedia.

                                </div>
                                @endforelse
                            </tbody>
                        </table>
                        <!-- End Table with stripped rows -->

                    </div>
                </div>

            </div>
        </div>
    </section>
    <script>
        //message with toastr
        @if(session() -> has('success'))
        toastr.success('{{ session('
            success ') }}', 'BERHASIL!');
        @elseif(session() -> has('error'))
        toastr.error('{{ session('
            error ') }}', 'GAGAL!');
        @endif
    </script>
</main><!-- End #main -->

@endauth
@endsection
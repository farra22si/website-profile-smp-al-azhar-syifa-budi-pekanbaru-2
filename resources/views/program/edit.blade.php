@extends('layouts.app-master')
@section('content')
@auth
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Program Sekolah</h1>
        <nav>
        <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('layouts.dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('program.index') }}">Tables</a></li>
                <li class="breadcrumb-item active">Edit Data</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Edit Program Sekolah</h5>
                        <form action="{{ route('program.update', $data->id_program) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="col-12">
                                <label class="form-label">Judul Program Sekolah</label>
                                <input type="text" class="form-control @error('nama_program') is-invalid @enderror" name="nama_program" placeholder="Masukkan Judul Program Sekolah" value="{{ $data->nama_program }}">
                                <!-- error message untuk nama_program -->
                                @error('nama_program')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Keterangan</label>
                                <textarea class="form-control @error('keterangan_program') is-invalid @enderror" placeholder="Masukkan Keterangan" name="keterangan_program">{{ $data->keterangan_program }}</textarea>
                                <!-- error message untuk nama_program -->
                                @error('keterangan_program')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Tanggal</label>
                                <input type="date" class="form-control @error('tanggal_program') is-invalid @enderror" name="tanggal_program" value="{{ $data->tanggal_program }}">
                                <!-- error message untuk tanggal_program -->
                                @error('tanggal_program')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Foto Program Sekolah</label>
                                <input type="file" class="form-control @error('foto_program') is-invalid @enderror" name="foto_program">
                                <!-- error message untuk title -->
                                @error('foto_program')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div> <br>
                            <div class="text-center">
                                <button type="submit" class="btn btn-md btn-primary">SIMPAN</button>
                                <button type="reset" class="btn btn-md btn-warning">RESET</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
        <script>
            CKEDITOR.replace('keterangan_program');
        </script>
    </section>
</main><!-- End #main -->
@endauth
@endsection
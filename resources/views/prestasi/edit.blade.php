@extends('layouts.app-master')
@section('content')
@auth
<main id="main" class="main">

    <div class="pagetitle">
        <h1>prestasi</h1>
        <nav>
        <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('layouts.dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('prestasi.index') }}">Tables</a></li>
                <li class="breadcrumb-item active">Edit Data</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Edit prestasi</h5>
                        <form action="{{ route('prestasi.update', $data->id_prestasi) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="col-12">
                                <label class="form-label">Nama Prestasi</label>
                                <input type="text" class="form-control @error('nama_prestasi') is-invalid @enderror" name="nama_prestasi" placeholder="Masukkan Nama Prestasi" value="{{ $data->nama_prestasi }}">
                                <!-- error message untuk nama_prestasi -->
                                @error('nama_prestasi')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Keterangan</label>
                                    <textarea class="form-control @error('keterangan_prestasi') is-invalid @enderror" placeholder="Masukkan Keterangan" name="keterangan_prestasi">{{ $data->keterangan_prestasi }}</textarea>
                                    <!-- error message untuk nama_prestasi -->
                                    @error('keterangan_prestasi')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Tanggal</label>
                                <input type="date" class="form-control @error('tanggal_prestasi') is-invalid @enderror" name="tanggal_prestasi" value="{{ $data->tanggal_prestasi }}">
                                <!-- error message untuk tanggal_prestasi -->
                                @error('tanggal_prestasi')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Foto Prestasi</label>
                                <input type="file" class="form-control @error('foto_prestasi') is-invalid @enderror" name="foto_prestasi">
                                <!-- error message untuk title -->
                                @error('foto_prestasi')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div> <br>
                            <div class="text-center">
                                <button type="submit" class="btn btn-md btn-primary">SIMPAN</button>
                                <button type="reset" class="btn btn-md btn-warning">RESET</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
        <script>
            CKEDITOR.replace('keterangan_prestasi');
        </script>
    </section>
</main><!-- End #main -->
@endauth
@endsection
@extends('layouts.app-master')
@section('content')
@auth
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Kontak</h1>
        <nav>
        <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('layouts.dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('tentang.index') }}">Tables</a></li>
                <li class="breadcrumb-item active">Tambah Data</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Tambah Kontak</h5>
                        <form action="{{ route('tentang.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="col-12">
                                <label class="form-label">Telepon</label>
                                <input type="text" class="form-control @error('telepon') is-invalid @enderror" name="telepon" placeholder="Masukkan Telepon">
                                <!-- error message untuk telepon -->
                                @error('telepon')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Instagram</label>
                                <input type="text" class="form-control @error('instagram') is-invalid @enderror" placeholder="Masukkan Instagram" name="instagram">
                                <!-- error message untuk instagram -->
                                @error('instagram')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Email</label>
                                <input type="text" class="form-control @error('email') is-invalid @enderror" placeholder="Masukkan Email" name="email">
                                <!-- error message untuk nama_program -->
                                @error('email')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Alamat</label>
                                <input type="text" class="form-control @error('alamat') is-invalid @enderror" placeholder="Masukkan Alamat" name="alamat">
                                <!-- error message untuk nama_program -->
                                @error('alamat')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Facebook</label>
                                <input type="text" class="form-control @error('facebook') is-invalid @enderror" placeholder="Masukkan Facebook" name="facebook">
                                <!-- error message untuk facebook -->
                                @error('facebook')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="text-center">
                                <button type="submit" class="btn btn-md btn-primary">SIMPAN</button>
                                <button type="reset" class="btn btn-md btn-warning">RESET</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main><!-- End #main -->
@endauth
@endsection
@extends('layouts.app-master')
@section('content')
@auth
<main id="main" class="main">

    <div class="pagetitle">
        <h1> Kalender Akademik </h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('layouts.dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item">Tables</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Kalender Akademik</h5>
                        <!-- Table with stripped rows -->
                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>TANGGAL</th>
                                    <th>KETERANGAN</th>
                                    <th>KALENDER 1</th>
                                    <th>KALENDER 2</th>
                                    <th>AKSI</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php($no = 1)
                                @forelse ($data as $kalender)
                                <tr class="text-center">
                                    <td>{{ $no ++ }}</td>

                                    <td>{{ $kalender->tanggal}}</td>

                                    <td>{!! $kalender->keterangan !!}</td>

                                    <td class="text-center">
                                        <img src="{{Storage::url('public/kalender/') . $kalender->file_1 }}" class="rounded" style="width: 150px">
                                    </td>

                                    <td class="text-center">
                                        <img src="{{Storage::url('public/kalender/') . $kalender->file_2 }}" class="rounded" style="width: 150px">
                                    </td>

                                    <td class="text-center">
                                        <form onsubmit="return confirm('Apakah Anda Yakin ?');" action="{{route('kalender.destroy', $kalender->id_kalender) }}" method="post">
                                            <a href="{{route('kalender.edit', $kalender->id_kalender) }}" class="btn btn-sm btn-primary">EDIT</a>

                                            @csrf
                                            @method('DELETE')

                                        </form>
                                    </td>
                                </tr>
                                @empty
                                <div class="alert alert-danger">
                                    Data Kalender Akademik belum

                                    Tersedia.

                                </div>
                                @endforelse
                            </tbody>
                        </table>
                        <!-- End Table with stripped rows -->

                    </div>
                </div>

            </div>
        </div>
    </section>
    <script>
        //message with toastr
        @if(session() -> has('success'))
        toastr.success('{{ session('
            success ') }}', 'BERHASIL!');
        @elseif(session() -> has('error'))
        toastr.error('{{ session('
            error ') }}', 'GAGAL!');
        @endif
    </script>
</main><!-- End #main -->

@endauth
@endsection
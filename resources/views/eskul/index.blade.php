@extends('layouts.app-master')
@section('content')
@auth
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Ekstrakulikuler</h1>
        <nav>
        <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('layouts.dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item">Tables</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Ekstrakulikuler Sekolah</h5>
                        <a href="{{ route('eskul.create') }}" class="btn btn-md btn-success mb-3">TAMBAH DATA EKSTRAKULIKULER </a>


                        <!-- Table with stripped rows -->
                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>NAMA</th>
                                    <th>KETERANGAN</th>
                                    <th>TANGGAL</th>
                                    <th>FOTO</th>
                                    <th>AKSI</th>

                                </tr>
                            </thead>
                            <tbody>
                            @php($no = 1)
                                @forelse ($data as $eskul)
                                <tr class="text-center">
                                    <td>{{ $no ++ }}</td>

                                    <td>{{ $eskul->nama_eskul}}</td>

                                    <td>{!! $eskul->keterangan_eskul !!}</td>

                                    <td>{{ $eskul->tanggal_eskul}}</td>

                                    <td class="text-center">
                                        <img src="{{Storage::url('public/eskul/') . $eskul->foto_eskul }}" class="rounded" style="width: 150px">

                                    </td>

                                    <td class="text-center">
                                        <form onsubmit="return confirm('Apakah Anda Yakin ?');" action="{{route('eskul.destroy', $eskul->id_eskul) }}" method="post">
                                            <a href="{{route('eskul.edit', $eskul->id_eskul) }}" class="btn btn-sm btn-primary">EDIT</a>

                                            @csrf
                                            @method('DELETE')

                                            <button type="submit" class="btn btn-sm btn-danger">HAPUS</button>
                                        </form>
                                    </td>
                                </tr>
                                @empty
                                <div class="alert alert-danger">
                                    Data eskul belum

                                    Tersedia.

                                </div>
                                @endforelse
                            </tbody>
                        </table>
                        <!-- End Table with stripped rows -->

                    </div>
                </div>

            </div>
        </div>
    </section>
    <script>
        //message with toastr
        @if(session() -> has('success'))
        toastr.success('{{ session('
            success ') }}', 'BERHASIL!');
        @elseif(session() -> has('error'))
        toastr.error('{{ session('
            error ') }}', 'GAGAL!');
        @endif
    </script>
</main><!-- End #main -->

@endauth
@endsection








@extends('layouts.app-master')
@section('content')
@auth
<main id="main" class="main">

    <div class="pagetitle">
        <h1> Data Visi Misi</h1>
        <nav>
        <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('layouts.dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('visimisi.index') }}">Tables</a></li>
                <li class="breadcrumb-item active">Tambah Data</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Tambah Data Visi Misi</h5>
                        <form class="row g-3" action="{{ route('visimisi.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="col-12">
                                <label class="form-label">Visi</label>
                                <textarea class="form-control @error('visi') is-invalid @enderror" rows="5" placeholder="Masukkan Visi" name="visi">{{ old('keterangan') }}</textarea>
                                <!-- error message untuk visi -->
                                @error('visi')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="col-12">
                                <label class="form-label">Misi</label>
                                <textarea class="form-control @error('misi') is-invalid @enderror" rows="5" placeholder="Masukkan Misi" name="misi">{{ old('keterangan') }}</textarea>
                                <!-- error message untuk misi-->
                                @error('misi')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div> <br>
                            <div class="text-center">
                                <button type="submit" class="btn btn-md btn-primary">SIMPAN</button>
                                <button type="reset" class="btn btn-md btn-warning">RESET</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
        <script>
            CKEDITOR.replace('visi');
            CKEDITOR.replace('misi');
        </script>
    </section>
</main><!-- End #main -->
@endauth
@endsection
@extends('layouts.app-master')
@section('content')
@auth
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Galeri</h1>
        <nav>
        <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('layouts.dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('galeri.index') }}">Tables</a></li>
                <li class="breadcrumb-item active">Tambah Data</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Tambah Galeri</h5>
                        <form action="{{ route('galeri.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="col-12">
                                <label class="form-label">Judul</label>
                                <input type="text" class="form-control @error('nama_galeri') is-invalid @enderror" name="nama_galeri" placeholder="Masukkan Judul">
                                <!-- error message untuk nama_galeri -->
                                @error('nama_galeri')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Keterangan</label>
                                <textarea class="form-control @error('keterangan_galeri') is-invalid @enderror" rows="5" placeholder="Masukkan Keterangan" name="keterangan_galeri">{{ old('keterangan_galeri') }}</textarea>
                                <!-- error message untuk nama_galeri -->
                                @error('keterangan_galeri')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Tanggal</label>
                                <input type="date" class="form-control @error('tanggal_galeri') is-invalid @enderror" name="tanggal_galeri">
                                <!-- error message untuk tanggal_galeri -->
                                @error('tanggal_galeri')
                                <div class=" alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">File</label>
                                <input type="file" class="form-control @error('file_galeri') is-invalid @enderror" name="file_galeri">
                                <!-- error message untuk title -->
                                @error('file_galeri')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div> <br>
                            <div class="text-center">
                                <button type="submit" class="btn btn-md btn-primary">SIMPAN</button>
                                <button type="reset" class="btn btn-md btn-warning">RESET</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
        <script>
            CKEDITOR.replace('keterangan_galeri');
        </script>
    </section>
</main><!-- End #main -->
@endauth
@endsection
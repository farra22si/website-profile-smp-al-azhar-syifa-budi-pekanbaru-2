@extends('layouts.app-master')

@section('content')
@auth
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Galeri</h1>
        <nav>
        <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('layouts.dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item">Tables</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Galeri</h5>
                        <a href="{{ route('galeri.create') }}" class="btn btn-md btn-success mb-3">TAMBAH DATA GALERI</a>
                        <!-- Table with stripped rows -->
                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>JUDUL</th>
                                    <th>KETERANGAN</th>
                                    <th>TANGGAL</th>
                                    <th>FILE</th>
                                    <th>AKSI</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php($no = 1)
                                @forelse ($data as $galeri)
                                <tr class="text-center">
                                    <td>{{ $no ++ }}</td>

                                    <td>{{ $galeri->nama_galeri}}</td>

                                    <td>{!! $galeri->keterangan_galeri !!}</td>

                                    <td>{{ $galeri->tanggal_galeri}}</td>

                                    <td class="text-center">
                                        <img src="{{Storage::url('public/galeri/') . $galeri->file_galeri }}" class="rounded" style="width: 150px">
                                    </td>

                                    <td class="text-center">
                                        <form onsubmit="return confirm('Apakah Anda Yakin ?');" action="{{route('galeri.destroy', $galeri->id_galeri) }}" method="post">
                                            <a href="{{route('galeri.edit', $galeri->id_galeri) }}" class="btn btn-sm btn-primary">EDIT</a>

                                            @csrf
                                            @method('DELETE')

                                            <button type="submit" class="btn btn-sm btn-danger">HAPUS</button>
                                        </form>
                                    </td>
                                </tr>
                                @empty
                                <div class="alert alert-danger">
                                    Data galeri belum

                                    Tersedia.

                                </div>
                                @endforelse
                            </tbody>
                        </table>
                        <!-- End Table with stripped rows -->

                    </div>
                </div>

            </div>
        </div>
    </section>
    <script>
        //message with toastr
        @if(session() -> has('success'))
        toastr.success('{{ session('
            success ') }}', 'BERHASIL!');
        @elseif(session() -> has('error'))
        toastr.error('{{ session('
            error ') }}', 'GAGAL!');
        @endif
    </script>
</main><!-- End #main -->

@endauth
@endsection
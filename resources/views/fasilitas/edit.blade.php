@extends('layouts.app-master')
@section('content')
@auth
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Sarana dan Prasarana<</h1>
        <nav>
        <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('layouts.dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('fasilitas.index') }}">Tables</a></li>
                <li class="breadcrumb-item active">Edit Data</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Edit Sarana dan Prasarana<</h5>
                        <form action="{{ route('fasilitas.update', $data->id_fasilitas) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="col-12">
                                <label class="form-label">Nama Sarana dan Prasarana<</label>
                                <input type="text" class="form-control @error('nama_fasilitas') is-invalid @enderror" name="nama_fasilitas" placeholder="Masukkan Nama Sarana dan Prasarana<" value="{{ $data->nama_fasilitas }}">
                                <!-- error message untuk nama_fasilitas -->
                                @error('nama_fasilitas')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Keterangan</label>
                                <textarea class="form-control @error('keterangan_fasilitas') is-invalid @enderror" placeholder="Masukkan Keterangan Sarana dan Prasarana<" name="keterangan_fasilitas">{{ $data->keterangan_fasilitas }}</textarea>
                                <!-- error message untuk nama_fasilitas -->
                                @error('keterangan_fasilitas')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Tanggal</label>
                                <input type="date" class="form-control @error('tanggal_fasilitas') is-invalid @enderror" name="tanggal_fasilitas" value="{{ $data->tanggal_fasilitas }}">
                                <!-- error message untuk tanggal_fasilitas -->
                                @error('tanggal_fasilitas')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Foto Sarana dan Prasarana<</label>
                                <input type="file" class="form-control @error('foto_fasilitas') is-invalid @enderror" name="foto_fasilitas">
                                <!-- error message untuk title -->
                                @error('foto_fasilitas')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div> <br>
                            <div class="text-center">
                                <button type="submit" class="btn btn-md btn-primary">SIMPAN</button>
                                <button type="reset" class="btn btn-md btn-warning">RESET</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
        <script>
            CKEDITOR.replace('keterangan_fasilitas');
        </script>
    </section>
</main><!-- End #main -->
@endauth
@endsection
@extends('home.app-master')
@section('content')

<!-- ======= Hero Section ======= -->
<section id="hero" class="hero">

  <div class="info d-flex align-items-center">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-6 text-center">
          <h2 data-aos="fade-down">Selamat Datang di <span>SMP Al-Azhar Syifa Budi Pekanbaru II</span></h2>
        </div>
      </div>
    </div>
  </div>

  <div id="hero-carousel" class="carousel slide" data-bs-ride="carousel" data-bs-interval="5000">
    @foreach ($data as $index)

    <div class="carousel-item active" style="background-image: url('{{ Storage::url('public/slider/' . $index->file) }}');"></div>

    @endforeach

    <a class="carousel-control-prev" href="#hero-carousel" role="button" data-bs-slide="prev">
      <span class="carousel-control-prev-icon bi bi-chevron-left" aria-hidden="true"></span>
    </a>

    <a class="carousel-control-next" href="#hero-carousel" role="button" data-bs-slide="next">
      <span class="carousel-control-next-icon bi bi-chevron-right" aria-hidden="true"></span>
    </a>

  </div>

</section><!-- End Hero Section -->

<main id="main">

  <!-- ======= Alt Services Section 2 ======= -->
  <section id="alt-services-2" class="alt-services">
    <div class="container" data-aos="fade-up">
      @foreach ($sejarah as $sejarahs)
      <div class="row justify-content-around gy-4">
        <div class="col-lg-5 d-flex flex-column justify-content-center">
          <h3>Profil SMP Al - Azhar Syifa Budi Pekanbaru II</h3>
          <p>
            {!! ( $sejarahs->keterangan) !!}
          </p>
        </div>
        <div class="col-lg-6 img-bg" style="background-image:  url('{{ Storage::url('public/sejarah/' . $sejarahs->file) }}');" data-aos="zoom-in" data-aos-delay="100"></div>

        @endforeach
      </div>
  </section><!-- End Alt Services Section 2 -->

  <!-- ======= Alt Services Section ======= -->
  <section id="alt-services" class="alt-services  section-bg">
    <div class="container" data-aos="fade-up">
      @foreach ($ucapan as $ucapans)
      <div class="row justify-content-around gy-4">
        <div class="col-lg-6 img-bg" style="background-image:  url('{{ Storage::url('public/ucapankepsek/' . $ucapans->foto) }}');" data-aos="zoom-in" data-aos-delay="100"></div>
        <div class="col-lg-5 d-flex flex-column justify-content-center">
          <h3>Ucapan Kepala Sekolah</h3>
          <p>
            {!! substr( $ucapans->keterangan , 0, 500) . '...' !!}
          </p>
          <a href="{{ route('home.ucapan') }}" class="readmore stretched-link">Lihat selengkapnya <i class="bi bi-arrow-right"></i></a>
        </div>
      </div>
      @endforeach
    </div>
  </section><!-- End Alt Services Section -->

  <!-- ======= Stats Counter Section ======= -->
  <section id="stats-counter" class="stats-counter">
    <div class="container">

      <div class="row gy-4">

        <div class="col-lg-3 col-md-6">
          <div class="stats-item d-flex align-items-center w-100 h-100">
            <i class="bi bi-mortarboard"></i>
            <div>
              <span data-purecounter-start="0" data-purecounter-end="400" data-purecounter-duration="1" class="purecounter"></span>
              <p>Siswa</p>
            </div>
          </div>
        </div><!-- End Stats Item -->

        <div class="col-lg-3 col-md-6">
          <div class="stats-item d-flex align-items-center w-100 h-100">
            <i class="bi bi-people color-pink flex-shrink-0"></i>
            <div>
              <span data-purecounter-start="0" data-purecounter-end="50" data-purecounter-duration="1" class="purecounter"></span>
              <p>Pendidik</p>
            </div>
          </div>
        </div><!-- End Stats Item -->

        <div class="col-lg-3 col-md-6">
          <div class="stats-item d-flex align-items-center w-100 h-100">
            <i class="bi bi-emoji-smile"></i>
            <div>
              <span data-purecounter-start="0" data-purecounter-end="15" data-purecounter-duration="1" class="purecounter"></span>
              <p>Karyawan</p>
            </div>
          </div>
        </div><!-- End Stats Item -->

        <div class="col-lg-3 col-md-6">
          <div class="stats-item d-flex align-items-center w-100 h-100">
            <i class="bi bi-buildings"></i>
            <div>
              <span data-purecounter-start="0" data-purecounter-end="20" data-purecounter-duration="1" class="purecounter"></span>
              <p>Sarana dan Prasarana</p>
            </div>
          </div>
        </div><!-- End Stats Item -->

      </div>

    </div>
  </section><!-- End Stats Counter Section -->

  <!-- ======= Services Section ======= -->
  <section id="services" class="services section-bg">
    <div class="container" data-aos="fade-up">

      <div class="row gy-12">

        <div class="col-lg-12 col-md-6" data-aos="fade-up" data-aos-delay="100">
          <div class="service-item  position-relative section-bg">
            <div class="icon">
              <i class="fa-solid fa-pen-to-square"></i>
            </div>
            <h3>Pendaftaran Peserta Didik Baru</h3>
            <p>Pendaftaran Peserta Didik Baru (PPDB) Tahun Akademik 2023/2024 dibuka.</p>
            <a href="http://alazhar.syifabudi.sch.id/admission" class="readmore stretched-link">Informasi selengkapnya <i class="bi bi-arrow-right"></i></a>
          </div>
        </div><!-- End Service Item -->

      </div>

    </div>
  </section><!-- End Services Section -->

  <!-- ======= Our Projects Section ======= -->
  <section id="projects" class="projects">
    <div class="container" data-aos="fade-up">

      <div class="portfolio-isotope" data-portfolio-filter="*" data-portfolio-layout="masonry" data-portfolio-sort="original-order">

        <div class="section-header">
          <h2>Program Sekolah</h2>
        </div>

        <div class="row gy-4 portfolio-container" data-aos="fade-up" data-aos-delay="200">
          @foreach ($program as $programs)
          <div class="col-lg-4 col-md-6 portfolio-item filter-remodeling">
            <div class="portfolio-content h-100">
              <img src="{{Storage::url('public/program/') . $programs->foto_program }}" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>{{ $programs->tanggal_program}}</h4>
                <p>{{ $programs->nama_program}}</p>
                <a href="{{Storage::url('public/program/') . $programs->foto_program }}" title="{{ $programs->keterangan_program}}" data-gallery="portfolio-gallery-remodeling" class="glightbox preview-link"><i class="bi bi-zoom-in"></i></a>
              </div>
            </div>
          </div><!-- End Projects Item -->
          @endforeach
        </div><!-- End Projects Container -->

      </div>

    </div>
  </section><!-- End Our Projects Section -->

  <!-- ======= Recent Blog Posts Section ======= -->
  <section id="recent-blog-posts" class="recent-blog-posts section-bg">
    <div class="container" data-aos="fade-up">

      <div class=" section-header">
        <h2>Informasi Terbaru</h2>
      </div>

      <div class="row gy-5">
        @foreach ($berita as $be)
        <div class="col-xl-4 col-md-6" data-aos="fade-up" data-aos-delay="100">
          <div class="post-item position-relative h-100">

            <div class="post-img position-relative overflow-hidden">
              <img src="{{Storage::url('public/berita/') . $be->foto_berita }}" class="img-fluid" alt="">
              <span class="post-date">{{ $be->tanggal_berita}}</span>
            </div>

            <div class="post-content d-flex flex-column">

              <h3 class="post-title">{{ $be->nama_berita}}</h3>

              <div class="meta d-flex align-items-center">
                <p>
                  {!! substr($be->keterangan_berita, 0, 50) . '...' !!}
                </p>
              </div>

              <hr>

              <a href="{{ route('home.isiberita', ['id_berita' => $be->id_berita]) }}" class="readmore stretched-link"><span>Read More</span><i class="bi bi-arrow-right"></i></a>

            </div>

          </div>
        </div><!-- End post item -->
        @endforeach
      </div>

    </div>
  </section>
  <!-- End Recent Blog Posts Section -->

</main><!-- End #main -->
@endsection
@extends('home.app-master')
@section('content')

<main id="main">

  <!-- ======= Breadcrumbs ======= -->
  @foreach ($galeri as $ga)
  @if ($ga->id_galeri == '8b3a5b02-aa6a-11ee-a529-8469936849d5')
  <div class="breadcrumbs d-flex align-items-center" style="background-image: url('{{ Storage::url('public/galeri/' . $ga->file_galeri) }}');">
    <div class="container position-relative d-flex flex-column align-items-center aos-init aos-animate" data-aos="fade">

      <h2>Ucapan Kepala Sekolah</h2>
      <ol>
        <li><a href="{{ route('home.index') }}">Beranda</a></li>
        <li>Ucapan Kepala Sekolah</li>
      </ol>

    </div>
  </div><!-- End Breadcrumbs -->
  @endif
  @endforeach

   <!-- ======= Alt Services Section ======= -->
   <section id="alt-services" class="alt-services  section-bg">
    <div class="container" data-aos="fade-up">
      @foreach ($ucapan as $ucapans)
      <div class="row justify-content-around gy-4">
        <div class="col-lg-6 img-bg" style="background-image:  url('{{ Storage::url('public/ucapankepsek/' . $ucapans->foto) }}');" data-aos="zoom-in" data-aos-delay="100"></div>
        <div class="col-lg-5 d-flex flex-column justify-content-center">
          <h3>Ucapan Kepala Sekolah</h3>
          <p>
          {!! $ucapans->keterangan !!}
          </p>
        </div>
      </div>
      @endforeach
    </div>
  </section><!-- End Alt Services Section -->

</main><!-- End #main -->

@endsection
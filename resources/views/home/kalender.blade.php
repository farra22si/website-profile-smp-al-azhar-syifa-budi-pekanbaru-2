@extends('home.app-master')
@section('content')

  <main id="main">
<!-- ======= Breadcrumbs ======= -->
@foreach ($galeri as $ga)
  @if ($ga->id_galeri == '8b3a5b02-aa6a-11ee-a529-8469936849d5')
  <div class="breadcrumbs d-flex align-items-center" style="background-image: url('{{ Storage::url('public/galeri/' . $ga->file_galeri) }}');">
    <div class="container position-relative d-flex flex-column align-items-center aos-init aos-animate" data-aos="fade">

      <h2>Kalender Akademik</h2>
      <ol>
        <li><a href="{{ route('home.index') }}">Beranda</a></li>
        <li>Kalender Akademik</li>
      </ol>

    </div>
  </div><!-- End Breadcrumbs -->
  @endif
  @endforeach

    <section id="features" class="features section-bg">
      <div class="container aos-init aos-animate" data-aos="fade-up">

        <ul class="nav nav-tabs row  g-2 d-flex" role="tablist">

          <li class="nav-item col-6" role="presentation">
            <a class="nav-link active show" data-bs-toggle="tab" data-bs-target="#tab-1" aria-selected="true" role="tab">
              <h2>SMP Al - Azhar Syifa Budi Pekanbaru II</h2>
            </a>
          </li><!-- End tab nav item -->

          <li class="nav-item col-6" role="presentation">
            <a class="nav-link" data-bs-toggle="tab" data-bs-target="#tab-2" aria-selected="false" tabindex="-1" role="tab">
              <h2>Al - Azhar Syifa Budi Pusat</h2>
            </a><!-- End tab nav item -->

        </ul>

        <div class="tab-content">
        @foreach ($kalender as $kalenders)
          <div class="tab-pane active show" id="tab-1" role="tabpanel">
            <div class="row">
              <div class="col-lg-14 order-2 order-lg-1 mt-3 mt-lg-0 d-flex flex-column justify-content-center aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
                <p class="fst-italic">
                <img src="{{Storage::url('public/kalender/') . $kalenders->file_1 }}" class="img-fluid" alt="" width="100%">
                </p>
              </div>
              <div class="col-lg-6 order-1 order-lg-2 text-center aos-init aos-animate" data-aos="fade-up" data-aos-delay="200">
                <img src="assets/img/features-1.jpg" alt="" class="img-fluid">
              </div>
            </div>
          </div><!-- End tab content item -->

          <div class="tab-pane" id="tab-2" role="tabpanel">
            <div class="row">
              <div class="col-lg-14 order-2 order-lg-1 mt-3 mt-lg-0 d-flex flex-column justify-content-center">
                <p class="fst-italic">
                <img src="{{Storage::url('public/kalender/') . $kalenders->file_2 }}" class="img-fluid" alt="" width="100%">
                </p>
              </div>
              <div class="col-lg-6 order-1 order-lg-2 text-center">
                <img src="assets/img/features-2.jpg" alt="" class="img-fluid">
              </div>
            </div>
          </div><!-- End tab content item -->
          @endforeach
        </div>

      </div>
    </section>


  </main><!-- End #main -->

  @endsection
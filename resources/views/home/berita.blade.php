@extends('home.app-master')
@section('content')

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
  @foreach ($galeri as $ga)
  @if ($ga->id_galeri == '8b3a5b02-aa6a-11ee-a529-8469936849d5')
  <div class="breadcrumbs d-flex align-items-center" style="background-image: url('{{ Storage::url('public/galeri/' . $ga->file_galeri) }}');">
    <div class="container position-relative d-flex flex-column align-items-center aos-init aos-animate" data-aos="fade">

      <h2>Informasi</h2>
      <ol>
        <li><a href="{{ route('home.index') }}">Beranda</a></li>
        <li>Informasi</li>
      </ol>

    </div>
  </div><!-- End Breadcrumbs -->
  @endif
  @endforeach

    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog">
      <div class="container" data-aos="fade-up" data-aos-delay="100">

        <div class="row gy-4 posts-list">
        @foreach ($data as $berita)
          <div class="col-xl-4 col-md-6">
          
            <div class="post-item position-relative h-100">
              <div class="post-img position-relative overflow-hidden">
                <img src="{{Storage::url('public/berita/') . $berita->foto_berita }}" class="img-fluid" alt="">
                <span class="post-date">{{ $berita->tanggal_berita}}</span>
              </div>
              <div class="post-content d-flex flex-column">
                <h3 class="post-title">{{ $berita->nama_berita}}</h3>
                <p>
                {!! substr($berita->keterangan_berita, 0, 100) . '...' !!}
                </p>
                <hr>
                <a href="{{ route('home.isiberita', ['id_berita' => $berita->id_berita]) }}" class="readmore stretched-link"><span>Read More</span><i class="bi bi-arrow-right"></i></a>
              </div>
            </div>
        
          </div><!-- End post list item -->
          @endforeach

        </div><!-- End blog posts list -->

        <div class="blog-pagination">
        {{ $data->links('home.partials.custom-pagination') }}
        </div><!-- End blog pagination -->

      </div>
    </section><!-- End Blog Section -->

  </main><!-- End #main -->

  @endsection
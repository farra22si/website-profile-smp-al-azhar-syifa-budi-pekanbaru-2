@extends('home.app-master')
@section('content')

<main id="main">

  <!-- ======= Breadcrumbs ======= -->
  @foreach ($galeri as $ga)
  @if ($ga->id_galeri == '8b3a5b02-aa6a-11ee-a529-8469936849d5')
  <div class="breadcrumbs d-flex align-items-center" style="background-image: url('{{ Storage::url('public/galeri/' . $ga->file_galeri) }}');">
    <div class="container position-relative d-flex flex-column align-items-center aos-init aos-animate" data-aos="fade">

      <h2>Organisasi</h2>
      <ol>
        <li><a href="{{ route('home.index') }}">Beranda</a></li>
        <li>Organisasi</li>
      </ol>

    </div>
  </div><!-- End Breadcrumbs -->
  @endif
  @endforeach

  <section id="alt-services" class="alt-services">

    <div class="container aos-init aos-animate" data-aos="fade-up">
      @foreach ($organisasi as $orga)
      <div class="row justify-content-around gy-4">
        <div class="col-lg-6 img-bg aos-init aos-animate" data-aos="zoom-in" data-aos-delay="100">
          <img src="{{Storage::url('public/organisasi/') . $orga->foto_organisasi }}" class="img-fluid" alt="" width="100%">
        </div>

        <div class="col-lg-5 d-flex flex-column justify-content-center">
          <h3>{{ $orga->nama_organisasi}}</h3>
          <p>{!! $orga->keterangan_organisasi !!}</p>
        </div>
      </div>
      @endforeach
    </div>
  </section>

</main>

@endsection
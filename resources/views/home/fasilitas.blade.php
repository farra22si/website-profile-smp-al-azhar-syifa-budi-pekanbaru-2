@extends('home.app-master')
@section('content')

<main id="main">

  <!-- ======= Breadcrumbs ======= -->
  <div class="breadcrumbs d-flex align-items-center" style="background-image: url('asset/img/breadcrumbs-bg.jpg');">
    <div class="container position-relative d-flex flex-column align-items-center aos-init aos-animate" data-aos="fade">

      <h2>Sarana dan Prasarana</h2>
      <ol>
        <li><a href="{{ route('home.index') }}">Beranda</a></li>
        <li>Sarana dan Prasarana</li>
      </ol>

    </div>
  </div><!-- End Breadcrumbs -->

  <section id="constructions" class="constructions">
    <div class="container aos-init aos-animate" data-aos="fade-up">

      <div class="section-header">
        <h2>Sarana dan Prasarana</h2>
      </div>

      <div class="row gy-4">
        @foreach ($data as $fasilitas)

        <div class="col-lg-6 aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
          <div class="card-item">
            <div class="row">
              <div class="col-xl-5">
                <img src="{{Storage::url('public/fasilitas/') . $fasilitas->foto_fasilitas }}" class="img-fluid" alt="">
              </div>
              <div class="col-xl-7 d-flex align-items-center">
                <div class="card-body">
                  <h4 class="card-title">{{ $fasilitas->nama_fasilitas}}</h4>
                  <p>{!! $fasilitas->keterangan_fasilitas !!}</p>
                </div>
              </div>
            </div>
          </div>
        </div><!-- End Card Item -->
        @endforeach

      </div>

    </div>
  </section>


</main><!-- End #main -->

@endsection
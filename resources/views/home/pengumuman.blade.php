@extends('home.app-master')
@section('content')

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs d-flex align-items-center" style="background-image: url('asset/img/breadcrumbs-bg.jpg');">
      <div class="container position-relative d-flex flex-column align-items-center" data-aos="fade">

        <h2>Pengumuman</h2>
        <ol>
          <li><a href="{{ route('home.index') }}">Beranda</a></li>
          <li>Pengumuman</li>
        </ol>

      </div>
    </div><!-- End Breadcrumbs -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container" data-aos="fade-up" data-aos-delay="100">

        <div class="row gy-4">
 isi
        </div>

        <div class="row gy-4 mt-1">
isi
        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  @endsection
@extends('home.app-master')
@section('content')

<main id="main">

  <!-- ======= Breadcrumbs ======= -->
  @foreach ($galeri as $ga)
  @if ($ga->id_galeri == '8b3a5b02-aa6a-11ee-a529-8469936849d5')
  <div class="breadcrumbs d-flex align-items-center" style="background-image: url('{{ Storage::url('public/galeri/' . $ga->file_galeri) }}');">
    <div class="container position-relative d-flex flex-column align-items-center aos-init aos-animate" data-aos="fade">

      <h2>Kontak</h2>
      <ol>
        <li><a href="{{ route('home.index') }}">Beranda</a></li>
        <li>Kontak</li>
      </ol>

    </div>
  </div><!-- End Breadcrumbs -->
  @endif
  @endforeach

  <!-- ======= Contact Section ======= -->
  <section id="contact" class="contact">
    <div class="container" data-aos="fade-up" data-aos-delay="100">
      @foreach ($tentang as $te)

      <div class="row gy-12">
        <div class="col-lg-3 col-md-8">
          <div class="info-item d-flex flex-column justify-content-center align-items-center">
          <i class="bi bi-geo-alt"></i>
            <h3>Alamat Kami</h3>
            <p>Jl. Letjend.S.Parman No.16, Suka Mulia, <br> Kec. Sail, Kota Pekanbaru, Riau 28127</p>
          </div>
        </div><!-- End Info Item -->

        <div class="col-lg-3 col-md-8">
          <div class="info-item d-flex flex-column justify-content-center align-items-center">
          <i class="bi bi-instagram"></i>
            <h3>Instagram Kami</h3>
            <p>{{ $te->instagram}}</p>
          </div>
        </div><!-- End Info Item -->

        <div class="col-lg-3 col-md-6">
          <div class="info-item d-flex flex-column justify-content-center align-items-center">
          <i class="bi bi-facebook"></i>
            <h3>Facebook Kami</h3>
            <p>{{ $te->facebook}}</p>
          </div>
        </div><!-- End Info Item -->

        <div class="col-lg-3 col-md-6">
          <div class="info-item  d-flex flex-column justify-content-center align-items-center">
            <i class="bi bi-telephone"></i>
            <h3>Telepon Kami</h3>
            <p>{{ $te->telepon}}</p>
          </div>
        </div><!-- End Info Item -->

      </div>

      <div class="row gy-4 mt-1">

        <div class="col-lg-12 ">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.654665535597!2d101.45593797496475!3d0.5189516994759711!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31d5ac2196f22555%3A0x438a07de71c34f65!2sSmp%20Al%20Azhar%20Syifa%20Budi%20Pekanbaru%20II!5e0!3m2!1sen!2sid!4v1702520055419!5m2!1sen!2sid" 
            width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
        </div><!-- End Google Maps -->

      </div>
      @endforeach
    </div>
  </section><!-- End Contact Section -->

</main><!-- End #main -->

@endsection
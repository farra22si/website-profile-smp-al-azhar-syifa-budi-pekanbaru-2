@extends('home.app-master')
@section('content')

<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    @foreach ($galeri as $ga)
    @if ($ga->id_galeri == '8b3a5b02-aa6a-11ee-a529-8469936849d5')
    <div class="breadcrumbs d-flex align-items-center" style="background-image: url('{{ Storage::url('public/galeri/' . $ga->file_galeri) }}');">
        <div class="container position-relative d-flex flex-column align-items-center aos-init aos-animate" data-aos="fade">

            <h2>Ekstrakulikuler</h2>
            <ol>
                <li><a href="{{ route('home.index') }}">Beranda</a></li>
                <li>Ekstrakulikuler</li>
            </ol>

        </div>
    </div><!-- End Breadcrumbs -->
    @endif
    @endforeach

    <section id="constructions" class="constructions">
        <div class="container aos-init aos-animate" data-aos="fade-up">

            <div class="section-header">
                <h2>Ekstrakulikuler</h2>
                <p>Ekstrakulikuler merupaka suatu kegiatan tambahan di khususkan buat siswa yang mana memiliki berbagai ragam jenis</p>
            </div>

            <div class="row gy-4">
                @foreach ($eskul as $es)

                <div class="col-lg-6 aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
                    <div class="card-item">
                        <div class="row">
                            <div class="col-xl-5">
                                <img src="{{Storage::url('public/eskul/') . $es->foto_eskul }}" class="img-fluid" alt="">
                            </div>
                            <div class="col-xl-7 d-flex align-items-center">
                                <div class="card-body">
                                    <h4 class="card-title">{{ $es->nama_eskul}}</h4>
                                    <p>{!! substr($es->keterangan_eskul, 0, 100) . '...' !!}</p>
                                    <a href="{{ route('home.detaileskul', ['id_eskul' => $es->id_eskul]) }}" class="readmore stretched-link">Baca selengkapnya <i class="bi bi-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- End Card Item -->
                @endforeach

            </div>

        </div>
    </section>

</main>

@endsection
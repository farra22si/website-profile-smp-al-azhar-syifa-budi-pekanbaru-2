@extends('home.app-master')
@section('content')

<main id="main">

  <!-- ======= Breadcrumbs ======= -->
  @foreach ($data as $ga)
  @if ($ga->id_galeri == '8b3a5b02-aa6a-11ee-a529-8469936849d5')
  <div class="breadcrumbs d-flex align-items-center" style="background-image: url('{{ Storage::url('public/galeri/' . $ga->file_galeri) }}');">
    <div class="container position-relative d-flex flex-column align-items-center aos-init aos-animate" data-aos="fade">

      <h2>Galeri</h2>
      <ol>
        <li><a href="{{ route('home.index') }}">Beranda</a></li>
        <li>Galeri</li>
      </ol>

    </div>
  </div><!-- End Breadcrumbs -->
  @endif
  @endforeach

  <!-- ======= Our Projects Section ======= -->
  <section id="projects" class="projects">
    <div class="container" data-aos="fade-up">

      <div class="portfolio-isotope" data-portfolio-filter="*" data-portfolio-layout="masonry" data-portfolio-sort="original-order">

        <div class="section-header">
          <h2>Galeri</h2>
        </div>

        <div class="row gy-4 portfolio-container" data-aos="fade-up" data-aos-delay="200">
          @foreach ($data as $galeri)
          <div class="col-lg-4 col-md-6 portfolio-item filter-remodeling">
            <div class="portfolio-content h-100">
              <img src="{{Storage::url('public/galeri/') . $galeri->file_galeri }}" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>{{ $galeri->tanggal_galeri}}</h4>
                <p>{{ $galeri->keterangan_galeri}}</p>
                <a href="{{Storage::url('public/galeri/') . $galeri->file_galeri }}" title="{{ $galeri->keterangan_galeri}}" data-gallery="portfolio-gallery-remodeling" class="glightbox preview-link"><i class="bi bi-zoom-in"></i></a>
              </div>
            </div>
          </div><!-- End Projects Item -->
          @endforeach
        </div><!-- End Projects Container -->

      </div>

    </div>
  </section><!-- End Our Projects Section -->

</main><!-- End #main -->

@endsection
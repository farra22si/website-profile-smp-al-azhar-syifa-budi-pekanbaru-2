@extends('home.app-master')
@section('content')

<main id="main">

  <!-- ======= Breadcrumbs ======= -->
  @foreach ($galeri as $ga)
  @if ($ga->id_galeri == '8b3a5b02-aa6a-11ee-a529-8469936849d5')
  <div class="breadcrumbs d-flex align-items-center" style="background-image: url('{{ Storage::url('public/galeri/' . $ga->file_galeri) }}');">
    <div class="container position-relative d-flex flex-column align-items-center aos-init aos-animate" data-aos="fade">

      <h2>Data Guru</h2>
      <ol>
        <li><a href="{{ route('home.index') }}">Beranda</a></li>
        <li>Data Guru</li>
      </ol>

    </div>
  </div><!-- End Breadcrumbs -->
  @endif
  @endforeach

  <!-- ======= Our Team Section ======= -->
  <section id="team" class="team">
    <div class="container aos-init aos-animate" data-aos="fade-up">

      <div class="section-header">
        <h2>Data Guru</h2>
        <p></p>
      </div>

      <div class="row gy-5">
        @foreach ($data as $guru)
        <div class="col-lg-3 col-md-6 member aos-init aos-animate" data-aos="fade-up" data-aos-delay="100" width="200px" height="250px">
          <div class="member-img">
            <img src="{{Storage::url('public/guru/') . $guru->foto_guru }}" class="img-fluid">
            
          </div>
          <div class="member-info text-center">
            <h4>{{ $guru->nama_guru}}</h4>
            <span>{{ $guru->jabatan}}</span>
            <p>{{ $guru->mapel}}</p>
          </div>
        </div><!-- End Team Member -->
        @endforeach

      </div>

    </div>
  </section><!-- End Our Team Section -->

</main><!-- End #main -->

@endsection
@extends('home.app-master')
@section('content')

<main id="main">

  <!-- ======= Breadcrumbs ======= -->
  @foreach ($galeri as $ga)
  @if ($ga->id_galeri == '8b3a5b02-aa6a-11ee-a529-8469936849d5')
  <div class="breadcrumbs d-flex align-items-center" style="background-image: url('{{ Storage::url('public/galeri/' . $ga->file_galeri) }}');">
    <div class="container position-relative d-flex flex-column align-items-center aos-init aos-animate" data-aos="fade">

      <h2>Informasi</h2>
      <ol>
        <li><a href="{{ route('home.index') }}">Beranda</a></li>
        <li><a href="{{ route('home.berita') }}">Informasi</a></li>
        <li>Penjelasan Informasi</li>
      </ol>

    </div>
  </div><!-- End Breadcrumbs -->
  @endif
  @endforeach

  <!-- ======= Blog Details Section ======= -->
  <section id="blog" class="blog">
    <div class="container" data-aos="fade-up" data-aos-delay="100">

      <div class="row g-5">

        <div class="col-lg-8">
        @if (isset($berita))
          <article class="blog-details">

            <div class="post-img text-center ">
              <img src="{{Storage::url('public/berita/') . $berita->foto_berita }}" class="img-fluid" alt="">
            </div>

            <h2 class="title">{{ $berita->nama_berita}}</h2>

            <div class="meta-top">
              <ul>
                <li class="d-flex align-items-center"><i class="bi bi-clock"></i> <a href="blog-details.html"><time datetime="2020-01-01">{{ $berita->tanggal_berita}}</time></a></li>
              </ul>
            </div><!-- End meta top -->

            <div class="content">
              <p>
                {!! $berita->keterangan_berita !!}
              </p>
            </div><!-- End post content -->

          </article><!-- End blog post -->
          @endif
        </div>

        <div class="col-lg-4">
      
          <div class="sidebar">

            <div class="sidebar-item recent-posts">
              <h3 class="sidebar-title">Informasi Terbaru</h3>
              @foreach ($recentPosts as $recentPost)
              <div class="mt-3">

                <div class="post-item mt-3">
                  <img src="asset/img/blog/blog-recent-1.jpg" alt="">
                  <div>
                    <h4><a href="{{ route('home.isiberita', ['id_berita' => $recentPost->id_berita]) }}">{{ $recentPost->nama_berita}}</a></h4>
                    <time datetime="2020-01-01">{{ $recentPost->tanggal_berita}}</time>
                  </div>
                </div><!-- End recent post item-->
              </div>
              @endforeach
            </div><!-- End sidebar recent posts-->
            <br>
            <a href="{{ route('home.berita') }}" class="readmore stretched-link">Lihat informasi lainnya <i class="bi bi-arrow-right"></i></a>

          </div><!-- End Blog Sidebar -->
        
        </div>
      </div>

    </div>
  </section><!-- End Blog Details Section -->

</main><!-- End #main -->

@endsection
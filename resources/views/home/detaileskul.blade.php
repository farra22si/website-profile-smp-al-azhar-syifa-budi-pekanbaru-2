@extends('home.app-master')
@section('content')

<main id="main">

  <!-- ======= Breadcrumbs ======= -->
  @foreach ($galeri as $ga)
  @if ($ga->id_galeri == '8b3a5b02-aa6a-11ee-a529-8469936849d5')
  <div class="breadcrumbs d-flex align-items-center" style="background-image: url('{{ Storage::url('public/galeri/' . $ga->file_galeri) }}');">
    <div class="container position-relative d-flex flex-column align-items-center aos-init aos-animate" data-aos="fade">

      <h2>Ekstrakulikuler</h2>
      <ol>
        <li><a href="{{ route('home.index') }}">Beranda</a></li>
        <li><a href="{{ route('home.eskul') }}">Ekstrakulikuler</a></li>
        <li>Penjelasan Ekstrakulikuler</li>
      </ol>

    </div>
  </div><!-- End Breadcrumbs -->
  @endif
  @endforeach

  <!-- ======= Blog Details Section ======= -->
  <section id="blog" class="blog">
    <div class="container" data-aos="fade-up" data-aos-delay="100">

      <div class="row g-5">

        <div class="col-lg-12">
        @if (isset($eskul))
          <article class="blog-details">

            <div class="post-img text-center">
              <img src="{{Storage::url('public/eskul/') . $eskul->foto_eskul }}" class="img-fluid" alt="">
            </div>

            <h2 class="title">{{ $eskul->nama_eskul}}</h2>

            <div class="content">
              <p>
                {!! $eskul->keterangan_eskul !!}
              </p>
            </div><!-- End post content -->

          </article><!-- End blog post -->
          @endif
        </div>

      </div>

    </div>
  </section><!-- End Blog Details Section -->

</main><!-- End #main -->

@endsection
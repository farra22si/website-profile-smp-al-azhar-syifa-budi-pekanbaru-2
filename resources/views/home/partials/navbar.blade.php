<header id="header" class="header d-flex align-items-center">
  <div class="container-fluid container-xl d-flex align-items-center justify-content-between">
  <link href="http://fonts.googleapis.com/css?family=Roboto" rel='stylesheet' type='text/css'>
    <a href="index.html" class="" width="100%" height="100%" style="font-size: 1.5rem; font-family:"Roboto", sans-serif;">
      <!-- Uncomment the line below if you also wish to use an image logo -->
      <img src="/asset/img/SMP_Al_-_Azhar_Syifa_Budi_Pekanbaru_II__2_-removebg-preview-removebg-preview.png" alt="" height="100px" > 
    </a>

    <i class="mobile-nav-toggle mobile-nav-show bi bi-list"></i>
    <i class="mobile-nav-toggle mobile-nav-hide d-none bi bi-x"></i>
    <nav id="navbar" class="navbar">
      <ul>
        <li><a href="{{ route('home.index') }}">Beranda</a></li>
        <li class="dropdown"><a href="#"><span>Tentang Kami</span> <i class="bi bi-chevron-down dropdown-indicator"></i></a>
          <ul>
            <li><a href="{{ route('home.ucapan') }}">Ucapan Kepala Sekolah</a></li>
            <li><a href="{{ route('home.guru') }}">Data Guru</a></li>
            <li><a href="{{ route('home.prestasi') }}">Prestasi</a></li>
            <li><a href="{{ route('home.fasilitas') }}">Sarana dan Prasarana</a></li>
            <li><a href="{{ route('home.galeri') }}">Galeri</a></li>
            <li><a href="{{ route('home.visimisi') }}">Visi dan Misi</a></li>
            <li><a href="{{ route('home.berita') }}">Informasi</a></li>
          </ul>
        </li>
        <li class="dropdown"><a href="#"><span>Akademik</span> <i class="bi bi-chevron-down dropdown-indicator"></i></a>
          <ul>
            <li><a href="{{ route('home.program') }}">Program Sekolah</a></li>
            <li><a href="{{ route('home.kalender') }}">Kalender Akademik</a></li>
          </ul>
        </li>
        <li class="dropdown"><a href="#"><span>Kemuridan</span> <i class="bi bi-chevron-down dropdown-indicator"></i></a>
          <ul>
            <li><a href="{{ route('home.organisasi') }}">Organisasi</a></li>
            <li><a href="{{ route('home.eskul') }}">Ekstrakulikuler</a></li>
          </ul>
        </li>
        <li><a href="{{ route('home.kontak') }}">Kontak</a></li>
      </ul>
    </nav><!-- .navbar -->

  </div>
</header><!-- End Header -->
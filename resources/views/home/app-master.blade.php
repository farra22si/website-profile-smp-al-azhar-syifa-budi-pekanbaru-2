<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>SMP Al-Azhar Syifa Budi Pekanbaru II</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="/asset/img/unnamed.png" rel="icon">
  <link href="/asset/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,600;1,700&family=Roboto:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Work+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="/asset/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="/asset/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="/asset/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="/asset/vendor/aos/aos.css" rel="stylesheet">
  <link href="/asset/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="/asset/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="/asset/css/main.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: UpConstruction
  * Updated: Sep 18 2023 with Bootstrap v5.3.2
  * Template URL: https://bootstrapmade.com/upconstruction-bootstrap-construction-website-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  @include('home.partials.navbar')
  @yield('content')

  <!-- ======= Footer ======= -->
  <footer id="footer" class="footer">

    <div class="footer-content position-relative">
      <div class="container">
        <div class="row">
          @foreach ($tentang as $te)
          <div class="col-lg-4 col-md-6">
            <div class="footer-info">
              <h3>SMP Al-Azhar Syifa Budi Pekanbaru II</h3>
              <p>
              {{ $te->alamat}}<br><br>
                <strong>Phone:</strong> {{ $te->telepon}}<br>
              </p>
              <div class="social-links d-flex mt-3">
                <a href="https://www.facebook.com/alazharpku/" class="d-flex align-items-center justify-content-center"><i class="bi bi-facebook"></i></a>
                <a href="https://www.instagram.com/smpalazharsyifabudipku/" class="d-flex align-items-center justify-content-center"><i class="bi bi-instagram"></i></a>
              </div>
            </div>
          </div><!-- End footer info column-->
          @endforeach

          <div class="col-lg-2 col-md-3 footer-links">
            <ul>
              <li style="font-size: 1.1em; font-weight: bold;"><a href="{{ route('home.index') }}">Beranda</a></li>
              <li style="font-size: 1.1em; font-weight: bold;"><a href="{{ route('home.ucapan') }}">Ucapan Kepala Sekolah</a></li>
              <li style="font-size: 1.1em; font-weight: bold;"><a href="{{ route('home.guru') }}">Data Guru</a></li>
            </ul>
          </div><!-- End footer links column-->

          <div class="col-lg-2 col-md-3 footer-links">
            <ul>
              <li style="font-size: 1.1em; font-weight: bold;"><a href="{{ route('home.prestasi') }}">Prestasi</a></li>
              <li style="font-size: 1.1em; font-weight: bold;"><a href="{{ route('home.fasilitas') }}">Sarana dan Prasarana</a></li>
              <li style="font-size: 1.1em; font-weight: bold;"><a href="{{ route('home.visimisi') }}">Visi dan Misi</a></li>
            </ul>
          </div><!-- End footer links column-->

          <div class="col-lg-2 col-md-3 footer-links">
            <ul>
              <li style="font-size: 1.1em; font-weight: bold;"><a href="{{ route('home.program') }}">Program Sekolah</a></li>
              <li style="font-size: 1.1em; font-weight: bold;"><a href="{{ route('home.kalender') }}">Kalender Akademik</a></li>
              <li style="font-size: 1.1em; font-weight: bold;"><a href="{{ route('home.berita') }}">Informasi</a></li>
            </ul>
          </div><!-- End footer links column-->

          <div class="col-lg-2 col-md-3 footer-links">
            <ul>
              <li style="font-size: 1.1em; font-weight: bold;"><a href="{{ route('home.organisasi') }}">Organisasi</a></li>
              <li style="font-size: 1.1em; font-weight: bold;"><a href="{{ route('home.eskul') }}">Ekstrakulikuler</a></li>
              <li style="font-size: 1.1em; font-weight: bold;"><a href="{{ route('home.kontak') }}">Kontak</a></li>
            </ul>
          </div><!-- End footer links column-->

        </div>
      </div>
    </div>

    <div class="footer-legal text-center position-relative">
      <div class="container">
        <div class="copyright">
          &copy;{{date('Y')}} Copyright <strong><span>SMP Al-Azhar Syifa Budi Pekanbaru II</span></strong>. All Rights Reserved
        </div>
      </div>
    </div>

  </footer>
  <!-- End Footer -->

  <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="/asset/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="/asset/vendor/aos/aos.js"></script>
  <script src="/asset/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="/asset/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="/asset/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="/asset/vendor/purecounter/purecounter_vanilla.js"></script>
  <script src="/asset/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="/asset/js/main.js"></script>

</body>

</html>
@auth
<!-- ======= Header ======= -->
<header id="header" class="header fixed-top d-flex align-items-center">

  <div class="d-flex align-items-center justify-content-between">
    <a href="{{ route('layouts.dashboard') }}" class="logo d-flex align-items-center">
      <span class="d-none d-lg-block">SMP AL-AZHAR SYIFA <br> BUDI PEKANBARU II</span>
    </a>
    <i class="bi bi-list toggle-sidebar-btn"></i>
  </div><!-- End Logo -->

  <nav class="header-nav ms-auto">
    <ul class="d-flex align-items-center">

      <li class="nav-item dropdown pe-3">
      @auth               
        <a class="nav-link nav-profile d-flex align-items-center pe-0" href="#" data-bs-toggle="dropdown">
          <img src="/assets/img/Gambardyva.jpg" alt="Profile" class="rounded-circle">
          <span class="d-none d-md-block dropdown-toggle ps-2">{{auth()->user()->username}}</span>
        </a><!-- End Profile Iamge Icon -->
        
        <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow profile">
          <li class="dropdown-header">
            <h6>{{auth()->user()->username}}</h6>
            <span>Admin</span>
          </li>
          <li>
            <hr class="dropdown-divider">
          </li>
          @endauth
          <li>
            <a class="dropdown-item d-flex align-items-center" href="users-profile.html">
              <i class="bi bi-person"></i>
              <span>My Profile</span>
            </a>
          </li>
          <li>
            <hr class="dropdown-divider">
          </li>

          {{auth()->user()->name}}
          <li>
            <a class="dropdown-item d-flex align-items-center" href="{{ route('logout.perform') }}">
              <i class="bi bi-box-arrow-right"></i>
              <span>Sign Out</span>
            </a>
          </li>

        </ul><!-- End Profile Dropdown Items -->
      </li><!-- End Profile Nav -->

    </ul>
  </nav><!-- End Icons Navigation -->

</header><!-- End Header -->

<!-- ======= Sidebar ======= -->
<aside id="sidebar" class="sidebar">

  <ul class="sidebar-nav" id="sidebar-nav">

    <li class="nav-heading">Main Menu </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href="{{ route('layouts.dashboard') }}">
        <i class="bi bi-speedometer2"></i>
        <span>Dashboard</span>
      </a>
    </li><!-- End Dashboard Nav -->

    <li class="nav-item">
      <a class="nav-link collapsed" href="{{ route('guru.index') }}">
        <i class="bi bi-database"></i>
        <span>Data Guru</span>
      </a>
    </li><!-- End Dashboard Nav -->


    <li class="nav-item">
      <a class="nav-link collapsed" href="{{ route('prestasi.index') }}">
        <i class="bi bi-trophy"></i>
        <span>Prestasi</span>
      </a>
    </li><!-- End Dashboard Nav -->
    <li class="nav-item">
      <a class="nav-link collapsed" href="{{ route('program.index') }}">
        <i class="bi bi-building"></i>
        <span>Program Sekolah</span>
      </a>
    </li><!-- End Dashboard Nav -->

    <li class="nav-item">
      <a class="nav-link collapsed" href="{{ route('berita.index') }}">
        <i class="bi bi-newspaper"></i>
        <span>Informasi</span>
      </a>
    </li><!-- End Dashboard Nav -->

    <li class="nav-item">
      <a class="nav-link collapsed" href="{{ route('organisasi.index') }}">
        <i class="bi bi-diagram-3"></i>
        <span>Organisasi</span>
      </a>
    </li><!-- End Dashboard Nav -->

    <li class="nav-item">
      <a class="nav-link collapsed" href="{{ route('eskul.index') }}">
        <i class="bi bi-file-ruled"></i>
        <span>Ekstrakulikuler</span>
      </a>
    </li><!-- End Dashboard Nav -->
  
    <li class="nav-item">
      <a class="nav-link collapsed" href="{{ route('fasilitas.index') }}">
        <i class="bi bi-buildings"></i>
        <span>Sarana dan Prasarana</span>
      </a>
    </li><!-- End Dashboard Nav -->

    <li class="nav-item">
      <a class="nav-link collapsed" href="{{ route('tentang.index') }}">
        <i class="bi bi-telephone"></i>
        <span>Kontak</span>
      </a>
    </li><!-- End Dashboard Nav -->

    <li class="nav-item">
      <a class="nav-link collapsed" href="{{ route('visimisi.index') }}">
        <i class="bi bi-lightbulb"></i>
        <span>Visi Misi</span>
      </a>
    </li><!-- End Dashboard Nav -->

    <li class="nav-item">
      <a class="nav-link collapsed" href="{{ route('ucapankepsek.index') }}">
        <i class="bi bi-person-square"></i>
        <span>Ucapan Kepala Sekolah</span>
      </a>
    </li><!-- End Dashboard Nav -->

    <li class="nav-item">
      <a class="nav-link collapsed" href="{{ route('sejarah.index') }}">
        <i class="bi bi-megaphone"></i>
        <span>Profil Sekolah</span>
      </a>
    </li><!-- End Dashboard Nav -->

    <li class="nav-item">
      <a class="nav-link collapsed" href="{{ route('kalender.index') }}">
      <i class="bi bi-calendar-date"></i>
        <span>Kalender Akademik</span>
      </a>
    </li><!-- End Dashboard Nav -->

    <li class="nav-heading">Galeri</li>
    <li class="nav-item">
      <a class="nav-link collapsed" href="{{ route('galeri.index') }}">
        <i class="bi bi-image"></i>
        <span>Galeri</span>
      </a>
    </li><!-- End Dashboard Nav -->

    <li class="nav-item">
      <a class="nav-link collapsed" href="{{ route('slider.index') }}">
        <i class="bi bi-images"></i>
        <span>Slider</span>
      </a>
    </li><!-- End Dashboard Nav -->

    <li class="nav-heading">Pengaturan</li>
    <li class="nav-item">
      <a class="nav-link collapsed" href="{{ route('admin.index') }}">
        <i class="bi bi-person"></i>
        <span>Data User</span>
      </a>
    </li><!-- End Dashboard Nav -->

    @guest
    <li class="nav-item">
      <a class="nav-link collapsed" href="{{ route('login.perform') }}">
        <i class="bi bi-box-arrow-in-right"></i>
        <span>Login</span>
      </a>
    </li><!-- End Login Page Nav -->

    <li class="nav-item">
      <a class="nav-link collapsed" href="{{ route('register.perform') }}">
        <i class="bi bi-card-list"></i>
        <span>Register</span>
      </a>
    </li><!-- End Register Page Nav -->
    @endguest

  </ul>

</aside><!-- End Sidebar-->
@endauth
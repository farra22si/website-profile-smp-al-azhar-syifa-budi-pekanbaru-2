@extends('layouts.app-master')

@section('content')
@auth
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Data Profil Sekolah</h1>
        <nav>
        <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('layouts.dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item">Tables</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Profil Sekolah</h5>
                        <a href="{{ route('profil.create') }}" class="btn btn-md btn-success mb-3">TAMBAH DATA PROFIL SEKOLAH</a>


                        <!-- Table with stripped rows -->
                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>NAMA SEKOLAH</th>
                                    <th>LOGO</th>
                                    <th>LOGO HEADER</th>
                                    <th>AKSI</th>

                                </tr>
                            </thead>
                            <tbody>
                            @php($no = 1)
                                @forelse ($data as $profil)
                                <tr class="text-center">
                                    <td>{{ $no ++ }}</td>

                                    <td>{{ $profil->nama_sekolah}}</td>

                                    <td class="text-center">
                                        <img src="{{Storage::url('public/profil/') . $profil->logo }}" class="rounded" style="width: 150px">

                                    </td>

                                    <td class="text-center">
                                        <img src="{{Storage::url('public/profil/') . $profil->logo_header }}" class="rounded" style="width: 150px">

                                    </td>

                                    <td class="text-center">
                                        <form onsubmit="return confirm('Apakah Anda Yakin ?');" action="{{route('profil.destroy', $profil->id_profil) }}" method="post">
                                            <a href="{{route('profil.edit', $profil->id_profil) }}" class="btn btn-sm btn-primary">EDIT</a>

                                            @csrf
                                            @method('DELETE')

                                            <button type="submit" class="btn btn-sm btn-danger">HAPUS</button>
                                        </form>
                                    </td>
                                </tr>
                                @empty
                                <div class="alert alert-danger">
                                    Data Profil Sekolah Belum Tersedia.

                                </div>
                                @endforelse
                            </tbody>
                        </table>
                        <!-- End Table with stripped rows -->

                    </div>
                </div>

            </div>
        </div>
    </section>
    <script>
        //message with toastr
        @if(session() -> has('success'))
        toastr.success('{{ session('
            success ') }}', 'BERHASIL!');
        @elseif(session() -> has('error'))
        toastr.error('{{ session('
            error ') }}', 'GAGAL!');
        @endif
    </script>
</main><!-- End #main -->

@endauth
@endsection
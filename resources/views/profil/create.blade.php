@extends('layouts.app-master')
@section('content')
@auth
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Profil Sekolah</h1>
        <nav>
        <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('layouts.dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('profil.index') }}">Tables</a></li>
                <li class="breadcrumb-item active">Tambah Data</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Tambah Profil Sekolah</h5>
                        <form action="{{ route('profil.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="col-12">
                                <label class="form-label">Nama Sekolah</label>
                                <input type="text" class="form-control @error('nama_sekolah') is-invalid @enderror" name="nama_sekolah" placeholder="Masukkan Nama Sekolah">
                                <!-- error message untuk nama_program -->
                                @error('nama_sekolah')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Logo</label>
                                <input type="file" class="form-control @error('logo') is-invalid @enderror" name="logo">
                                <!-- error message untuk title -->
                                @error('logo')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Logo Header</label>
                                <input type="file" class="form-control @error('logo_header') is-invalid @enderror" name="logo_header">
                                <!-- error message untuk title -->
                                @error('logo_header')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <br>
                            <div class="text-center">
                                <button type="submit" class="btn btn-md btn-primary">SIMPAN</button>
                                <button type="reset" class="btn btn-md btn-warning">RESET</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main><!-- End #main -->

@endauth
@endsection
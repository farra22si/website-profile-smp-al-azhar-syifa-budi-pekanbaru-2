-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 09, 2024 at 03:09 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `profil_alazhar`
--

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `nama_berita` varchar(255) NOT NULL,
  `id_berita` varchar(255) NOT NULL,
  `tanggal_berita` date NOT NULL DEFAULT current_timestamp(),
  `keterangan_berita` text NOT NULL,
  `foto_berita` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`nama_berita`, `id_berita`, `tanggal_berita`, `keterangan_berita`, `foto_berita`) VALUES
('Memperingati Assyura 1445', '0c9546c6-ad45-11ee-82e0-8469936849d5', '2023-07-27', '<p>BUBUR ASYURA<br />\r\nAda banyak cerita dalam sejarah Islam yang terjadi di bulan Muharram (salah satu bulan yang Allah muliakan), salah satunya adalah yang terjadi di tanggal 10 bulan Muharram atau disebut dengan Asyura.<br />\r\nRagam kegiatan ummat Islam lakukan di tanggal tersebut disamping tentunya adalah puasa Sunnah di tanggal 10<br />\r\nbulan Muharram.<br />\r\nBubur Asyura merupakan bagan dr tradisi masyarakat</p>', '3dWIuAG8L7szATGyfCwndIHZ8A7u1pQSRy20Jjgx.jpg'),
('Pengambilan Raport 2023', '0e11850f-ad42-11ee-82e0-8469936849d5', '2023-12-21', '<p>Pengambilan Rapor Sumatif dan Penilaian Akhir Semester<br />\r\nGanjil T.P 2023/2024</p>', 'KFTe0NqKvJVr5C9pvrP3DhmoLzCm7ZJrsKb8Pksn.jpg'),
('Gebyar Maulid Nabi 1445', '0f87e2f1-ad44-11ee-82e0-8469936849d5', '2023-10-04', '<p>Gebyar Maulid Nabi Muhammad SAW 1445 H<br />\r\nUntuk mengembangkan bakat-bakat hebat dan membentuk mental percaya diri dalam menampilkan sebuah karya. Kami pihak sekolah mengadakan perlombaan untuk memeriahkan kegiatan Maulid Nabi Muhammad SAW 1445 H dengan perlombaan&quot; sederhana seperti<br />\r\n1. Lomba Adzan<br />\r\n2. Lomba Tahfizh<br />\r\n3. Lomba Story Telling<br />\r\n4. Lomba Pidato Bahasa Arab<br />\r\n5. Lomba Pidato Bahasa Indonesia</p>', 'voadMI4ci8flFKXN9Rzx5Xjjh5Bej5Ui4RVhFkah.jpg'),
('Hari Pahlawan 10 November 2023', '177a4621-ad43-11ee-82e0-8469936849d5', '2023-11-10', '<p>Kami segenap Keluarga Besar Al Azhar Syifa Budi<br />\r\nPekanbaru |I Mengucapkan Selamat Hari Pahlawan 10<br />\r\nNovember 2023</p>', 'Q68ybnSz8pQjCCWQxZSKZnG8use0EC4RkmfY2KPe.jpg'),
('Sedekah Perbukaan Gratis (SPG) 2023', '180292df-ad48-11ee-82e0-8469936849d5', '2023-03-30', '<p>SPG (Sedekah Perbukaan Gratis)<br />\r\n&quot;Barang siapa yang member hidangan berbuka untuk orang yang berpuasa maka dia akan mendapatkan pahala seperti orang yang berpuasa itu, tapa mengurangi pahala orang yang berpuasa itu sama sekali.&quot; (HR. Tirmidzi. Dia berkata,<br />\r\n&quot;Hadits hasan shahih.&quot;).<br />\r\nBulan Ramadhan adalah bulan dilipat gandakannya pahala orang beriman, maka ketika diberi kesempatan bertemu ramadhan mari kita isi dengan perbanyak ibadah kepadaNya.<br />\r\nSalah satu nilai pahala dari amal shaleh di bulan Ramadhan adalah memberi makan utk org berbuka, karena akan mendapat pahala sebanyak orang yang kita beri makanan berbuka.<br />\r\nMari suport program SPG dengan mengirimkan donasi ke rekening Bank Riau Kepri syariah di nomor (820 40 88888)</p>', 'mFG8lTBnPNcbRvpGTFLzkQ0iM0S9q6DlLJjUE0xO.jpg'),
('Ujian Tengah Semester 2023', '2f80c2ca-ad44-11ee-82e0-8469936849d5', '2023-09-22', '<p>Penilaian Tengah Semester<br />\r\nSebagai upaya mengetahui keberhasilan pembelajaran dalam tengah semester perlu diadakan sebuah evaluasi. Keberhasilan sebuah evaluasi adalah jika dijalankan dengan penuh kejujuran, dengan instrumen yang tepat dan pengawasan yang ketat.</p>', 'T8auQqbFsuDa4E06xOEjToEYaXwN0ixBTsQyzxBP.jpg'),
('Ujian Tahfidz Ummi 2023', '4345dee0-ad42-11ee-82e0-8469936849d5', '2023-11-29', '<p>Ujian Tahfizh Ummi ini bertujuan untuk melakukan pengujian hafaln al - quran siswa/i SMP AL AZHAR SYIFA BUDI PEKANBARU II</p>', 'q0ux6JhxfGY7BiVhmarnPPBvuxMkqLpomJ8pBK9m.jpg'),
('EID ADHA MUBARAK 1445', '4a167886-ad47-11ee-82e0-8469936849d5', '2023-06-28', '<p>Keluarga besar SMP SMA Al Azhar Syifa Budi Pekanbaru I mengucapkan Selamat Hari Raya Idul Adha 1444 H / 2023M</p>', 'zhNghcnmpIRY5rrXApkROTUhhNVX8nOUeXEMvj9v.jpg'),
('Acara Nonton Bareng \"Satu Hari Bersama Ibu\"', '544cea05-ad44-11ee-82e0-8469936849d5', '2023-09-28', '<p>Apresiasi Film Kebaikkan &quot;Satu<br />\r\nHari dengan Ibu&quot;. Keluarga Besar SMP - SMA Al Azhar Syifa Budi Pekanbaru Il. Selagi ibu mash ada, manfaatkan waktu bersamanya.<br />\r\nJangan biarkan waktu berlalu, baru tersadar ingin selalu bersamanya. Ibu kalau sudah pergi, tak akan pernah kembali. Alhamdulillah di hari Kamis, 28 September 2023. Nobar bersama keluarga besar SMP - SMA Al Azhar Syifa Budi Pekanbaru Il di XXI Mall SKA Pekanbaru</p>', '3DfrtaPYXRiq9UUkhh0CwyyFBsnhyxIEamM5Q89Y.jpg'),
('Isra\' Mi\'raj 1444', '573a4750-ad48-11ee-82e0-8469936849d5', '2023-02-18', '<p>Peringatan Isra&#39; Miraj dengan melakukan ibadah di bulan Rajab in tidak hanya untuk mengingatkan kita kembali pada ketaqwaan, tetapi juga sarana dalam mengapresiasi pengetahuan kita tentang mana dan hikmah dari Isra&#39; Miraj. Selamat memperingati Isra&#39; Miraj 1444 H. Semoga kita semua mendapat rahmat Allah SWT. Aamiin Allahumma Aamin</p>', 'DTJwiOeSSEA0hJ9fXUEXKoKU6ws0kupSutPQvMgC.jpg'),
('Gelar Karya Memperingati Hari Pancasila', '6a43bb1c-ad42-11ee-82e0-8469936849d5', '2023-11-18', '<p>Salah satu karakteristik Kurikulum Merdeka adalah Projek Penguatan Profil Pelajar Pancasila (P5). P5 berfokus pada pengembangan kompetensi dan karakter pelajar melalui belajar kelompok seputar isu penting dalam konteks nyata di sekitarnya</p>', 'Z2tNLiXGrQ4DGwScrGzI6XIddjUcvzpaMfmX0FQ8.jpg'),
('Memperingati Tahun Baru Islam 1445', '6df72394-ad46-11ee-82e0-8469936849d5', '2023-07-18', '<p>Dzikir dan Do&#39;a Menyambut Tahun Baru Islam 1445 H SMP<br />\r\n- SMA Al Azhar Syifa Budi Pekanbaru II<br />\r\n&quot;Momentum Hijrah sebagai Spirit Perubahan Mandiri<br />\r\nMenuju Insan yang Berprestasi&quot;</p>', 'cdGCwmQdVGzB5DyNMXeRM7eHGwRpkwyvyUgNil49.jpg'),
('Wisuda Akbar Kelas Tahfidz 2023', '6eab7724-ad47-11ee-82e0-8469936849d5', '2023-06-09', '<p>Wisuda Akbar<br />\r\n4 Kelas Tahfidz Qur&#39;an SMP-SMA AI - Azhar Syifa Budi Pekanbaru I Bersama Ustadz Abdul Shomad dan Pimpinan Keagamaan<br />\r\nASB Jakarta Ustadz Muhammad Ridwan<br />\r\nHari : Jum&#39;at 9 Juni 2023<br />\r\nTempat : Hotel Pengeran Pekanbaru<br />\r\nJam : 07.00 wib - selesai</p>', 'wXitKlxNS4Tq4PU27DpdAEi9zgqk1MwH6l5Jurcy.jpg'),
('Back To School 2023-2024', '92b564f2-ad46-11ee-82e0-8469936849d5', '2023-07-12', '<p>Alhamdulillah libur panjang kita sudah usai. Mari saatnya kita menyambut Tahun Ajaran Baru dengan semangaaaat baru</p>', 'oaPJGtRLeDNe72Aafu7OuZHLcIgcYBv658U6cVmW.jpg'),
('YAUM AL - TAKHARRUJ Angkatan XIV Tahun Ajaran 2022/2023', '92e7bbcc-ad47-11ee-82e0-8469936849d5', '2023-06-10', '<p>YAUM AL - TAKHARRUJ<br />\r\nAngkatan XIV Tahun Ajaran 2022/2023<br />\r\nSMP AL AZHAR SYIFA BUDI PEKANBARU I<br />\r\nHari/Tanggal : Sabtu, 10 Juni 2023<br />\r\nTempat : Hotel Labersa Pekanbaru<br />\r\nJam : 13.00 wib - selesai<br />\r\nAlhamdulillah kegiatan Yaumul Al-Thakharruj Angkatan XIV tahun ajaran 2022/2023 pada hari Sabtu, 10 Juni 2023 di Labersa Hotel Pekanbaru, berjalan dengan lancar. Semoga ananda yang wisuda menjadi anak yang sukses kedepannya. Aamin Allahumma Aamiin</p>', 'Xb8Us5Y5lQyq0xvFXX0z1ZUGGb06UBS8T73H6hkq.jpg'),
('Students conduct the English Day program by playing Kahoot! 2023', 'aa96c658-ad47-11ee-82e0-8469936849d5', '2023-05-25', '<p>Students conduct the English Day program by playing Kahoot!</p>', 'tFlMZBToudJvmA9uTq0Q37cFTVGAcqRfWuyVIJct.jpg'),
('TRY OUT Assesmen Kompetensi Minimum AKM 2023', 'ae3e6ae7-ad48-11ee-82e0-8469936849d5', '2023-02-07', '<p>Alhamdulillah, kegiatan Try Out Assesmen Kompetensi Minimum (UKM) bersama Eduleb hari ini Selasa, 7 Februari 2023 berjalan dengan lancari</p>', 'wR0aItBudS4nyKzjXks8ejt5Mq5rquGETWxqHTtS.jpg'),
('Implementasi Kurikulum Merdeka 2023', 'b672a95d-ad46-11ee-82e0-8469936849d5', '2023-06-24', '<p>Alhamdulillah seluruh rangkaian kegiatan IN HOUSE<br />\r\nTRAINING (IHT) guru-guru SMP SMA Al Azhar Syifa Budi<br />\r\nPekanbaru telah selesai dilaksanakan.<br />\r\nMari kita bergerak bersama demi kesuksesan anak bangsa menuju Kurikulum Merdeka</p>', 'yn1CrJ9NEIKeCyyLrNfIdCKezmIt3EsatovP9KhJ.jpg'),
('Program Apcom 2023', 'ca40c4b8-ad44-11ee-82e0-8469936849d5', '2023-09-02', '<p>Alhamdulillah Sabtu, 2 September 2023 telah terlaksanakan presentasi program dari para wali murid APCOM &quot;AI Azhar Syifa Budi Parent Comunity&quot; setiap angkatan mulai dari tingkatan SMP sampai dengan tingkatan SMA Semoga program&quot; yang telah dipresentasikan dan diajukan para wali murid kepihak sekolah, dapat terlaksanakan satu persatu</p>', 'EVeipsvMXl3tHwMOGT4V6iCJoARw6mweADSsuHZy.jpg'),
('Silaturahmi Wali Murid Kelas Tahfidz 2023', 'd7167d4e-ad47-11ee-82e0-8469936849d5', '2023-05-13', '<p>Silaturahmi Wali Murid kelas Tahfizh yang dilaksanakan pada hari Sabtu, 13 Mei 2023 berjalan dengan lancar. Jazakumullahu khairan para wali murid yang telah hadir dan berpartisipasi dalam kegiatan ini</p>', 'yy52G8CNclMyV0GYIOmbqWCNTUIjQ0hnl6k2Ikzh.jpg'),
('Memperingati 17 Agustus 2023', 'e37b6837-ad44-11ee-82e0-8469936849d5', '2023-08-17', '<p>Dalam rangka memperingati<br />\r\nHUT RI KE 78 SMP - SMA Al Azhar Syifa Budi Pekanbaru melaksanakan upacara bendera.<br />\r\nPerjuangan Bangsa Indonesia bukan hanya dari masa lalu, hari ini, hari esok, dan selamanya. Perjuangan kita belum berakhir. Mari kita perjuangkan bersama Indonesia Adil dan Sejahtera.<br />\r\nSelamat Hari Kemerdekaan!<br />\r\nMari terus berkarya dan menjadi kebanggaan tanah air.<br />\r\nMerdeka Merdeka Merdeka!!!</p>', 'm6fa2WNNswIImqqVWp1XPGNbpXuHggwZeIjNgOfc.jpg'),
('Gema Mulid Nabil Tahun 1445', 'ed94897b-ad43-11ee-82e0-8469936849d5', '2023-10-06', '<p>Bertepatan di Hari Jum&#39;at, 6 Oktober 2023 telah terlaksanakan kegiatan Gema Maulid Nabi Muhammad<br />\r\nSAW 1445 H yang dilaksanakan di aula bersama siswa siswi SMP - SM Al Azhar Syifa Budi Pekanbaru II.</p>', 'tUyeHITrf2BRFKHM6vqoPuvREuEPhV0F3z8BUcV7.jpg'),
('Palestina Memanggil 2023', 'f1cbe737-ad42-11ee-82e0-8469936849d5', '2023-11-06', '<p>Dalam rangka menumbuhkan rasa kepedulian terhadap saudara kita yang sedan teraniaya di Palestina dan sebagai bentuk kecintaan kita terhadap bumi Palestina yang saat ini memanggil kita untuk peduli kepada mereka semua.<br />\r\nMaka SMP dan SMA Al Azhar Syifa Budi Pekanbaru Il akan melakukan aksi penggalangan dana PALESTINA<br />\r\nMEMANGGIL yang insyaAllah kita lakukan penggalangan sejak Senin sampai Jumat 06 - 10 November 2023.<br />\r\nSetiap harinya pengurus OSIS akan mengedarkan kotak infak di sat penjemputan dan pengumpulan infak setiap harinya untuk siswa di sekolah.<br />\r\nKe seluruhan donasi sejak Senin sampai Jumat akan kita serahkan dalam aks penggalangan dana di Masjid An Nur bekerjasama dengan BAZNAS Propinsi Riau pada hari<br />\r\nJum&#39;at tg| 10 November 2023<br />\r\nMari suport dan tunjukan kepedulian kita terhadap saudara kita di sana. Allah Maha Menyaksikan aksi nyata dari kepedulian kita terhadap saudara kita yang teraniaya di sana.<br />\r\nSemoga Allah mudahkan jalan kita dan Allah tolong saudara kita di Palestina. Aamin</p>', 'vIZ1WW6DQFP7Jmtdu8h32sJ9t08EtEzHO2qtslBG.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `eskul`
--

CREATE TABLE `eskul` (
  `id_eskul` varchar(255) NOT NULL,
  `nama_eskul` varchar(255) NOT NULL,
  `keterangan_eskul` text NOT NULL,
  `foto_eskul` varchar(255) NOT NULL,
  `tanggal_eskul` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `eskul`
--

INSERT INTO `eskul` (`id_eskul`, `nama_eskul`, `keterangan_eskul`, `foto_eskul`, `tanggal_eskul`) VALUES
('1498f8c2-ad8c-11ee-82e0-8469936849d5', 'Basket', '<p>Kegiatan ekstrakurikuler bola basket merupakan salah satu kegiatan di luar jam pelajaran sekolah dan sebagai wahana untuk menampung, menyalurkan dan membina minat, bakat serta kegemaran peserta didik dalam mengikuti kegiatan ekstrakurikuler.&nbsp;Hal ini&nbsp;dapat menjadi salah satu motivasi peserta didik dalam mengikuti setiap&nbsp;kegiatan ekstrakurikuler.&nbsp;Kegiatan esktrakurikuler bola basket di Irfani QBS&nbsp;dilaksanakan satu kali dalam satu minggu, yaitu pada hari Sabtu.&nbsp;</p>\r\n\r\n<p>Bola basket merupakan cabang olahraga beregu di mana bola basket dimainkan oleh dua regu yang terdiri atas lima pemain untuk masing-masing regu dengan tujuan untuk mencetak angka sebanyak-banyaknya.&nbsp;Seperti dijelaskan dalam peraturan permainan, bola basket dimainkan oleh dua regu yang masing-masing regu terdiri dari lima pemain, setiap regu berusaha untuk memasukkan bola ke dalam keranjang lawan yang mencegah regu lawan memasukkan bola atau mencetak angka.</p>', 'JY88xX8j2BQmV23IU9uHI1ZUtKOrNhskhVZQz0WJ.jpg', '2024-01-08'),
('523c1b75-ad8c-11ee-82e0-8469936849d5', 'Futsal', '<p>Ekstrakurikuler futsal adalah kegiatan ekstrakurikuler di sekolah yang menawarkan kesempatan bagi siswa untuk belajar dan mengembangkan keterampilan dalam bermain futsal. Futsal adalah olahraga yang mirip dengan sepak bola, namun dimainkan dalam ruangan yang lebih kecil dan dengan jumlah pemain yang lebih sedikit.</p>\r\n\r\n<p>Di ekstrakurikuler futsal, siswa akan diajarkan tentang teknik dasar dalam bermain futsal, seperti teknik dribble, passing, shooting, dan teknik pertahanan. Selain itu, siswa juga akan belajar tentang taktik dan strategi dalam permainan futsal, serta mempelajari aturan dan etika dalam bermain futsal.</p>\r\n\r\n<p>Kegiatan ekstrakurikuler futsal di sekolah juga dapat membantu siswa untuk meningkatkan keterampilan motorik, keseimbangan, koordinasi, dan kebugaran fisik. Para siswa juga dapat belajar untuk bekerja sama dalam tim, meningkatkan keterampilan sosial dan keterampilan berkomunikasi.</p>', 'QuzfPpFei6mHuK8bF0WoTCMPj7YWypgQrWCnIBNZ.jpg', '2024-01-08'),
('8522e049-ad8c-11ee-82e0-8469936849d5', 'Cheerleader', '<p>Cheerleaders atau pemandu sorak layaknya sebuah kata yang sering kita dengar sebagai salah satu penyemarak dan penyemangat dalam beberapa kegiatan olahraga bergengsi. Namun dalam kenyataannya, kini nama tersebut telah menjelma menjadi salah satu cabang olahraga yang diakui secara umum. Tidak lagi dipandang sebelah mata sebagai sekumpulan pemudi&nbsp;<strong><em>&ldquo;centil&rdquo;</em></strong>&nbsp;yang hanya bersorak dengan keras dan memberikan semangat pada tim olahraga yang sedang berlaga saja. Kini cheerleading &nbsp;telah berevolusi menjadi salah satu cabang olahraga yang telah diakui eksistensinya dan layak diperhitungkan dalam dunia olahraga.</p>\r\n\r\n<p>Substansi cheerleading yang dewasa kini lebih didominasi dengan gerakan&nbsp;<em>gymnastic, tumblings, jumpings, stunts dan pyramids</em>&nbsp;yang sangat membutuhkan kemampuan fisik prima dalam setiap&nbsp;<em>performance</em>&nbsp;telah dapat membuktikan bahwa cheerleading merupakan sebuah olahraga&nbsp;<strong><em>&ldquo;modern extreme&rdquo;</em></strong>&nbsp;yang cukup digemari oleh remaja. Namun tak hanya bersubstansikan pyramids dan stunts yang fantastis, cheerleading tetap dikemas dengan ekspresi dan dance motion yang cantik dan menarik untuk disaksikan.</p>\r\n\r\n<p>Sebagai salah satu cabang olahraga baru cheerleading tentunya masih memiliki berbagai kekurangan. Diantaranya adalah kurangnya pengorganisiran dan pencarian bakat untuk regenerasi serta masalah pendanaan .</p>', '5WXJQbNL9W8URlgelpcctysSUdQ0hYGKKDMtc8Zh.jpg', '2024-01-08');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fasilitas`
--

CREATE TABLE `fasilitas` (
  `id_fasilitas` varchar(255) NOT NULL,
  `nama_fasilitas` varchar(255) NOT NULL,
  `keterangan_fasilitas` text NOT NULL,
  `foto_fasilitas` varchar(255) NOT NULL,
  `tanggal_fasilitas` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `fasilitas`
--

INSERT INTO `fasilitas` (`id_fasilitas`, `nama_fasilitas`, `keterangan_fasilitas`, `foto_fasilitas`, `tanggal_fasilitas`) VALUES
('1439ed4b-ad51-11ee-82e0-8469936849d5', 'Tempat Wudhu', '<p>Tempat wudhu di sekolah memiliki fungsi yang penting bagi siswa muslim yang ingin melaksanakan sholat. Berwudhu merupakan salah satu cara bersuci yang dilakukan untuk membersihkan anggota tubuh dari hadas kecil.</p>', 'FcIbJpzlRg9RBRbL8Gm9ARzA97R6F9Xe0ycETzEE.jpg', '2024-01-07'),
('1b4c27be-ad50-11ee-82e0-8469936849d5', 'Aula', '<p>Aula merupakan sebuah ruangan yang memiliki&nbsp;ukuran yang besar dan luas. Ruangan ini sering digunakan untuk mengadakan acara-acara khusus seperti seminar, rapat, presentasi, kegiatan keagamaan, kegiatan belajar mengajar, dan kesenian</p>', 'FBbcD2rMBHyrnLwHzZ7U32U6mmETfJDPntCy46qC.jpg', '2024-01-07'),
('249b81ae-ad65-11ee-82e0-8469936849d5', 'Ruang Ummi', '<p>Ruang Ummi&nbsp;di sekolah biasdigunakan sebagai tempat untuk menghafal dan mempelajari Al-Quran.&nbsp;</p>', 'Gqh0nEcMZ0mJtcdeLeoGC8aFgYhQ4Pl5Qd7q7MQP.jpg', '2024-01-07'),
('34b784cd-ad64-11ee-82e0-8469936849d5', 'Ruang Kelas', '<p>Ruang kelas adalah ruangan yang digunakan sebagai tempat belajar dan mengajar di sekolah, ruang ini sering kali menjadi tempat yang paling sering dikunjungi oleh para siswa dan guru. Ruang kelas memiliki peran yang sangat penting dalam proses belajar mengajar.</p>', 'JNuxI1RPHrg9SDo2eDifMNW8pnL97qGCawzjDZCl.jpg', '2024-01-07'),
('428a9133-ad4f-11ee-82e0-8469936849d5', 'UKS', '<p>UKS atau Usaha Kesehatan Sekolah adalah program pemerintah yang bertujuan untuk meningkatkan pelayanan kesehatan, pendidikan kesehatan, dan pembinaan lingkungan sekolah sehat atau kemampuan hidup sehat bagi warga sekolah. UKS berperan dalam memberikan pengetahuan yang berkaitan dengan masalah-masalah kesehatan pada peserta didik.</p>', 'V0FJZ58PDzNKYkGpE5vgSMHpRs423tYhTHDp2q9C.jpg', '2024-01-07'),
('98db53aa-ad4c-11ee-82e0-8469936849d5', 'Kantin', '<p>Kantin adalah sebuah ruangan dalam sebuah gedung sekolah yang dapat digunakan untuk murid, pengajar, staf sekolah, dan orang umum untuk membeli makanan, memakan makanan yang telah dibawa untuk dimakan dimeja yang telah disediakan</p>', '3c63SY7eiPoRSTMBBfuGZKXvpB8PvqhgP0yvDw6o.jpg', '2024-01-07'),
('a6ef5ee1-ad50-11ee-82e0-8469936849d5', 'Ruang Musik', '<p>Ruang Musik adalah ruangan yang menyediakan keperluan bermusik dan ruang bagi komunitas musik di sekolah. Fungsi ruang musik adalah untuk memperoleh pengetahuan ilmu musik dan sebagai tempat untuk mengekspresikan diri sebagai pecinta musik</p>', 'BPSSzNsvmxDKzPDUfKIF1ooC7Nl3DY5aS8aqlxgu.jpg', '2024-01-07'),
('c4286b5f-ad63-11ee-82e0-8469936849d5', 'Ruang Tamu', '<p>Ruang tamu berfungsi sebagai tempat menunggu bagi tamu yang datang ke sekolah, seperti pejabat, orang tua siswa atau tamu undangan</p>', 'b0bM52aEK8gEjxduXBfjC2AdXsjdbPFDGAXjBvGH.jpg', '2024-01-07'),
('cfd3ce56-ad4e-11ee-82e0-8469936849d5', 'Toilet', '<p>Toilet adalah fasilitas yang digunakan untuk membuang sisa-sisa metabolisme (feses dan urine) dari tubuh manusia<br />\r\n&nbsp;</p>', 'SoNgir6yFTFqczyXIzaYzibhuNcN3Twl9tcRIxFj.jpg', '2024-01-07'),
('e5340e68-ad4b-11ee-82e0-8469936849d5', 'Lorong Kelas', '<p>Lorong merupakan suatu ruangan&nbsp;yang memberikan akses jalan pada ruang non-jalan atau dapat diartikan lorong adalah penghubung dua ruangan atau lebih pada suatu tempat&nbsp;yang berfungsi memberikan ruang untuk berjalan dari satu ruangan ke ruangan lain</p>', 'AsZasvnlBQV17DLrxqWH6hYD2bL6HJNEXv2J9UrX.jpg', '2024-01-07');

-- --------------------------------------------------------

--
-- Table structure for table `galeri`
--

CREATE TABLE `galeri` (
  `id_galeri` varchar(255) NOT NULL,
  `nama_galeri` varchar(255) NOT NULL,
  `tanggal_galeri` date NOT NULL DEFAULT current_timestamp(),
  `keterangan_galeri` text NOT NULL DEFAULT current_timestamp(),
  `file_galeri` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `galeri`
--

INSERT INTO `galeri` (`id_galeri`, `nama_galeri`, `tanggal_galeri`, `keterangan_galeri`, `file_galeri`) VALUES
('052fbb26-ad74-11ee-82e0-8469936849d5', '34wtf3', '2019-05-29', '<p>23wtfr</p>', '6dft0UTkDAopyStAkkDWUiGSl5iRoufrVgoLT9QI.jpg'),
('15dc35b5-ad74-11ee-82e0-8469936849d5', 'mabit', '2018-02-21', '<p>mabit</p>', 'qa3SEk8QSwsKxxkFJOZEXodhNv99dFp0KYxf8KJN.jpg'),
('17be8a46-aa18-11ee-a529-8469936849d5', 'Guru', '2024-01-03', 'Guru', 'lCltZBiqILzqXmCmZHpmiidappqzlt8CNLHgVfca.jpg'),
('244225b2-ad74-11ee-82e0-8469936849d5', 'ergw4g32', '2018-07-25', '<p>fw3t32t2qa</p>', 'wbL8gFSl8HfgtNVI4L0holu75Fx0IeAC2LTx9ntW.jpg'),
('285e201e-ad73-11ee-82e0-8469936849d5', 'khatam', '2023-09-20', '<p>khatam</p>', '0FbDpRDkwiAekdTDTDKC08A2NbHZxLqevwIWtELn.jpg'),
('34eef3f5-ad74-11ee-82e0-8469936849d5', 'aya', '2019-05-29', '<p>aya</p>', 'gxbr4PV05yyjCU16PGaCCguPYPFCVeLqSZuzGczI.jpg'),
('4234de38-ad73-11ee-82e0-8469936849d5', 'perpisahan', '2023-09-26', '<p>perpisahan</p>', 'WyatZVO6LJ3TrcLITMBCRn4bDqd5CQXecKy8Rd0S.jpg'),
('44071ee7-ad74-11ee-82e0-8469936849d5', 'wt234t', '2018-06-22', '<p>2t23qrqaa</p>', 'oOzUjnWtQYsiyhtM2UQnNTgW9rLuwFvTngix1qpr.jpg'),
('5348049c-ad74-11ee-82e0-8469936849d5', 'ewt', '2018-06-20', '<p>ww4t4</p>', 'tKAiOHm7hfkofABvYcKExxPtERe9naH3nZsMQk4O.jpg'),
('5abcad40-ad73-11ee-82e0-8469936849d5', 'foto5', '2023-09-18', '<p>foto5</p>', 'PsWUNyfmmRJWEEEX2yokabndKEr2a2KwtaVj5sbH.jpg'),
('68fc4a33-ad74-11ee-82e0-8469936849d5', 'rwfw', '2018-06-13', '<p>wtfww3</p>', 'X3qI3LGx8NvsjL26pZeqJH3blwiWyDiN5obBamDi.jpg'),
('6aded5bd-ad73-11ee-82e0-8469936849d5', 'wer', '2022-07-11', '<p>weer</p>', 'kvULFsDdHdUi7qKlJRboKalWcLqerU8rinThIZH3.jpg'),
('81df24a7-ad74-11ee-82e0-8469936849d5', 'dsvsdvdv', '2018-06-22', '<p>aefveaaeg</p>', 'TH6wwEXTQCEsaM7CPa6mGlOPNEp5kvKAxz8BO2aK.jpg'),
('83f82bc9-ad73-11ee-82e0-8469936849d5', '123', '2022-08-10', '<p>234</p>', '1QlzsJY6haPnFy66yHeLdd91fIWhcop1nzZ1j5IB.jpg'),
('8b3a5b02-aa6a-11ee-a529-8469936849d5', 'Sekolah', '2024-01-04', 'Sekolah', '4STEp2yq7QWusIAwsXkHgTPVRU5ANaO4QOVDyUWq.jpg'),
('8f131ec4-ad72-11ee-82e0-8469936849d5', 'foto1', '2017-08-25', '<p>foto1</p>', 'n8R6nZRMiQBE6gOLeNHOSATLIDUJRIz0guredUX8.jpg'),
('92125f6f-ad74-11ee-82e0-8469936849d5', 'efwqefgf', '2019-02-13', '<p>qfqafqafaz</p>', 'RG94uMpdya7Ex3SdUoMASjOND7wInqu6DmUukSgG.jpg'),
('9320e019-ad73-11ee-82e0-8469936849d5', 'adfadfa', '2022-04-16', '<p>1244g</p>', 'Myfps4AnKQN6R8LluXyjVZUW1wIp6Htp640EL2Xu.jpg'),
('bb570ede-ad73-11ee-82e0-8469936849d5', '3r2t4', '2022-06-10', '<p>ergerwt34</p>', 'nyojN9iM3m6OmaP0T0kFh5H927qvecNG8ZHFyErZ.jpg'),
('c28b4552-ad74-11ee-82e0-8469936849d5', 'rt4ttq3', '2018-05-22', '<p>3wr3t3qt3w2</p>', 'a8pzARWe624TtmU1GVGv0YqsHE9Jnstc0smtgibM.jpg'),
('c4749346-ad72-11ee-82e0-8469936849d5', 'foto2', '2021-06-25', '<p>foto2</p>', 'zUARFbX26BToVCJ34WxWJi8AShdWPZxU6DJsuCxE.jpg'),
('d18e0e8d-ad74-11ee-82e0-8469936849d5', 'ergt34t4', '2018-10-25', '<p>tt4t4t4t2</p>', 'xBKvgR1UrP0NuEuc5QrAOImWBvSqoymCmzyMx2F4.jpg'),
('d5ea8e2a-ad73-11ee-82e0-8469936849d5', 'et34t', '2022-07-20', '<p>tgwg5h3</p>', 'fANIHK4irUoFzEXG3Aa9lBFm2HJFyOPluyILe3UD.jpg'),
('db947e0b-ad72-11ee-82e0-8469936849d5', 'foto3', '2021-05-25', '<p>foto3</p>', 'HhQaAWyNHyvCvoFefxlN7KS1xG6gIw8UxbBcNrt9.jpg'),
('e0f0a1f1-ad74-11ee-82e0-8469936849d5', '5t34tt', '2018-12-18', '<p>4234t34tw3</p>', 'Sx1sUnu7bJ4rJxFQ2qsdrAc9Rr7CHAlcW1aVGYRj.jpg'),
('e7025aa7-ad73-11ee-82e0-8469936849d5', 't34tt34', '2021-05-22', '<p>4t34t4</p>', 'hn3Ci6xAYohCtoXacAND56rWRLh8jyPW78cTdNOW.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE `guru` (
  `id_guru` varchar(255) NOT NULL,
  `nama_guru` varchar(255) NOT NULL,
  `jabatan` varchar(255) NOT NULL,
  `mapel` varchar(255) NOT NULL,
  `foto_guru` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`id_guru`, `nama_guru`, `jabatan`, `mapel`, `foto_guru`) VALUES
('0617ab5f-ad6f-11ee-82e0-8469936849d5', 'Nora Aprina, M.Pd', 'Wakil Kurikulum', 'Ilmu Pengetahuan Alam', 'adt7gEA9qcl4amzOuiqkwO8NzI4Vr6rQPMlpbVsM.jpg'),
('0c223106-ad6d-11ee-82e0-8469936849d5', 'Dedi Kamseno, S.Pd', 'Guru Bidang Studi', 'Agama', '58V8goJWrgViD6Yq2xJCk3CALBIFohGHExLcDydp.jpg'),
('0c29ac84-ad70-11ee-82e0-8469936849d5', 'Wahyu Rahmadi Fitra, S.Pd', 'Guru Bidang Studi', 'Pendidikan Jasmani', 'tZPHyGZ6ctNSiKvEaP6fA1nADxDIWB1AfOwam31B.jpg'),
('2038faea-ad71-11ee-82e0-8469936849d5', 'Nora Zulmiati, S.Pd', 'Guru Bidang Studi', 'Bahasa Indonesia', 'GPbrP8jc0dKUmyeXVtykKGnRsNVZbUJcgT0K4fSm.jpg'),
('2135a09b-ad6e-11ee-82e0-8469936849d5', 'Jeki Siswanto, SH', 'Guru Bidang Studi', 'Al-Quran', 'TpwuOKMQlDtg1TEIfB5PyBZf4uA2qyCB3NCLcdjl.jpg'),
('2ea25c57-ad6d-11ee-82e0-8469936849d5', 'Dr.Suhaimi Agus, M.Pd.I', 'Guru Bidang Studi', 'Agama', 'nDcRRvpdsAsTUPnzHuI7uesjTxmp1RAKGjTy9xhl.jpg'),
('3251858d-ad70-11ee-82e0-8469936849d5', 'Yoleta Rachman,S.Si,Gr', 'Guru Bidang Studi', 'Matematika', 'Yh1L53QxJ9y73XLbTK5G9n6SGfOy0YOjjMXM8Cw4.jpg'),
('391372b2-ad6f-11ee-82e0-8469936849d5', 'Noverina Sasrrealty, S.Psi', 'Bimbingan Konseling', 'Bimbingan Konseling', 'uatHlDgTD7xkcNYolaE2kyYZvsyj1YhYuGhL8RU5.jpg'),
('48c64fa9-ad6e-11ee-82e0-8469936849d5', 'Kartika Santi, S.Pd', 'Guru Bidang Studi', 'Ilmu Pengetahuan Alam', 'YNfJk794aIMlUFAgeOYx9YM01PIkPhZJ0ZA1Xbnv.jpg'),
('4a87dbf0-ad70-11ee-82e0-8469936849d5', 'Yuli Ifda, MA', 'Guru Bidang Studi', 'Agama', '41gqSYTpF9NfSRKdXZFWuurqN5KlfaDgY92gZCzC.jpg'),
('4f909af7-ad6d-11ee-82e0-8469936849d5', 'Efrizal Tasrul, S.Pd', 'Guru Bidang Studi', 'Bahasa Inggris', 'KvqI6o1QD3h3SEhXJKOo1z5WLSy5hIgV470G2fSH.jpg'),
('530d1a1e-ad6f-11ee-82e0-8469936849d5', 'Rahmawilis, S.Pd', 'Guru Bidang Studi', 'Ilmu Pengetahuan Sosial', 'IEK8eMIT7m1ewfDzaIagEyPQD7kr1fFpJ6nGol6U.jpg'),
('62849218-ad6e-11ee-82e0-8469936849d5', 'Kiki Mandala, S.Pd', 'Guru Bidang Studi', 'Ilmu Pengetahuan Alam', 'SiPouAhj5jEduB4UB0uVwqztgB0RDGmfPbv26jlz.jpg'),
('64a94c2d-ad70-11ee-82e0-8469936849d5', 'Yuliani Mukarramah, S.Pd', 'Guru Bidang Studi', 'Bahasa indonesia', 'MtyJP4XowS4iXdQBNIiknn0MwTH6C6jniPvJ9830.jpg'),
('6a5f52cd-ad6d-11ee-82e0-8469936849d5', 'Etri Kumala Sari. S.Pd', 'Guru Bidang Studi', 'Seni', 'mzvGnMzFEv9FYeRMtJMHSQ8ZV3brhxtZIzBqaHZ6.jpg'),
('72b469ad-ad6f-11ee-82e0-8469936849d5', 'Reffy Elinda, S.Pd, Gr', 'Guru Bidang Studi', 'Bahasa Inggris', 'fEQCPg75PJOUXd8NwZMHciSgyVEJ3Qh5mwwnt68B.jpg'),
('7fe20afd-ad6e-11ee-82e0-8469936849d5', 'Lulu Diani Ifadati, SE', 'Guru Bidang Studi', 'Ilmu Pengetahuan Sosial', 'MCXNlV7zf08ARmo38H5lcVSFEDt3jFNtKTXin03g.jpg'),
('83daceb1-ad6d-11ee-82e0-8469936849d5', 'Fadila Rahmi, S.Pd', 'Guru Bidang Studi', 'Seni', 'GmfsdqB1VukuGclZExIkUR2wzp3Vs7zPreQQ7wAo.jpg'),
('910b0401-ad6f-11ee-82e0-8469936849d5', 'Reza Listiawan, S.Ap', 'Guru Bidang Studi', 'Al-Quran', 'l1iYFR162xY9OJgM0iExqJWAxm6ULsGZA4kzj6T3.jpg'),
('9c480083-ad6e-11ee-82e0-8469936849d5', 'Mahar Al Malik, A.Md. Sn', 'Guru Bidang Studi', 'Seni', 'Fa5IlYVH4QjkPHChlYDDP529tIwNSSQy299BEgKJ.jpg'),
('a2b0547b-ad6d-11ee-82e0-8469936849d5', 'Fadliawati, S.Pd', 'Guru Bidang Studi', 'IPS', 'I2cLMJO3cdTIwkrTnckL5ocNUxRccDIWxuqn6n6q.jpg'),
('b25621cf-ad6f-11ee-82e0-8469936849d5', 'Siti Fatimah, S.Pd', 'Guru Bidang Studi', 'Al-Quran', 'gGlOlb69g1c7dfZg5tyJ0gcryq2cOtad1KJLozzF.jpg'),
('b80bb1e6-ad6e-11ee-82e0-8469936849d5', 'Mawaddah Warahmah, S.Pd', 'Guru Bidang Studi', 'Al-Quran', 'GaryVRBVnyqLqA39DmzzcmRQwxZSV2j10rKmP3c0.jpg'),
('bc440acb-ad6c-11ee-82e0-8469936849d5', 'Afniza Darsaf, S.Pd', 'Guru Bidang Studi', 'Matematika', 'JjmhpWXML466jd2XWYWjGAkzRJBcUHyXQPp5ra0c.jpg'),
('bf670d3e-ad6d-11ee-82e0-8469936849d5', 'H. Kurtubi, S.Ag', 'Kepala Sekolah', 'Agama', 'S9g8lmGkEw4KRQ4lxMtkQzID9XYD1bUseGMHviE4.jpg'),
('d306eb2c-ad6f-11ee-82e0-8469936849d5', 'Syahrul Ramadhani, S.Pd', 'Wakil Kesiswaan', 'Pendidikan Jasmani', 'CQrVuyKeNy5FAzkfQKThLXv5DQb9E1zeiApTrwhK.jpg'),
('e02b26c8-ad6d-11ee-82e0-8469936849d5', 'Indah Ningsih, S.Pd', 'Guru Bidang Studi', 'Pendidikan Kewarganegaraan', 'yFfpGyfP47ltVMI2myl2a56Rp5xvgOOFt3MqJYPX.jpg'),
('e56e3b48-ad6e-11ee-82e0-8469936849d5', 'Nike Wahyuni, M.Psi', 'Bimbingan Konseling', 'Bimbingan Konseling', 'ECAhBKqbLJMLYBLY9NaCGcnUMtZwf2Fd4LRInPX7.jpg'),
('f103ac86-ad6f-11ee-82e0-8469936849d5', 'Tari Eka Purnama, S.Pd', 'Guru Bidang Studi', 'Matematika', 'Et8Gph4cyuPEulygD6eQXqSH1Iymmvshuw8vaZ53.jpg'),
('f20c99ad-ad6c-11ee-82e0-8469936849d5', 'Astari, S.Pd', 'Guru Bidang Studi', 'Bahasa Indonesia', 'JuU1U3LJDPnnm0nmeyaIYx11xJXN47pAk9wN9k2T.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `kalender`
--

CREATE TABLE `kalender` (
  `id_kalender` varchar(255) NOT NULL,
  `tanggal` date NOT NULL DEFAULT current_timestamp(),
  `keterangan` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `kalender`
--

INSERT INTO `kalender` (`id_kalender`, `tanggal`, `keterangan`, `file`) VALUES
('49ea7d8f-ae85-11ee-aa2d-8469936849d5', '2024-01-09', '<p>Tahun Ajaran 2023/2024</p>', '9xHwmrkMeJfPjrvBTBQc75H8wauc87SR1jTLyZxM.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `organisasi`
--

CREATE TABLE `organisasi` (
  `id_organisasi` varchar(255) NOT NULL,
  `nama_organisasi` varchar(255) NOT NULL,
  `keterangan_organisasi` text NOT NULL,
  `foto_organisasi` varchar(255) NOT NULL,
  `tanggal_organisasi` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `organisasi`
--

INSERT INTO `organisasi` (`id_organisasi`, `nama_organisasi`, `keterangan_organisasi`, `foto_organisasi`, `tanggal_organisasi`) VALUES
('a10f8806-ad8b-11ee-82e0-8469936849d5', 'Organisasi Siswa Intra Sekolah (OSIS)', '<p>OSIS bertugas mengatur kegiatan-kegiatan yang berhubungan dengan kepentingan siswa di sekolah, serta memfasilitasi kegiatan-kegiatan ekstrakurikuler yang diinginkan oleh siswa.</p>\r\n\r\n<p>OSIS juga bertugas membantu menjaga kebersihan dan keamanan di sekolah, serta membantu menjalankan kegiatan-kegiatan sosial yang bermanfaat bagi masyarakat sekitar. Organisasi ini biasanya terdiri dari siswa yang terpilih melalui proses pemilihan yang diadakan di sekolah, dan dipimpin oleh ketua OSIS yang juga merupakan siswa.</p>\r\n\r\n<p>OSIS memiliki tujuan untuk membantu siswa dalam memahami dan menghargai peran mereka dalam masyarakat, serta memberikan kesempatan kepada siswa untuk belajar tentang tanggung jawab, kepemimpinan, dan kerjasama. Selain itu, OSIS juga bertujuan untuk meningkatkan kepedulian siswa terhadap masalah-masalah sosial dan membantu mereka dalam memecahkan masalah-masalah tersebut.</p>', 'jPehv6lMHavoHICkcf5bnXw41a303msCFSvsq3g3.jpg', '2024-01-08');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pengumuman`
--

CREATE TABLE `pengumuman` (
  `id_pengumuman` varchar(255) NOT NULL,
  `nama_pengumuman` varchar(255) NOT NULL,
  `tanggal_pengumuman` date NOT NULL DEFAULT current_timestamp(),
  `keterangan_pengumuman` text NOT NULL,
  `foto_pengumuman` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `prestasi`
--

CREATE TABLE `prestasi` (
  `id_prestasi` varchar(255) NOT NULL,
  `nama_prestasi` varchar(255) NOT NULL,
  `tanggal_prestasi` date NOT NULL DEFAULT current_timestamp(),
  `keterangan_prestasi` text NOT NULL,
  `foto_prestasi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `prestasi`
--

INSERT INTO `prestasi` (`id_prestasi`, `nama_prestasi`, `tanggal_prestasi`, `keterangan_prestasi`, `foto_prestasi`) VALUES
('1733eb08-ad79-11ee-82e0-8469936849d5', 'Muhammad Raziq Arif Dermawan', '2022-09-09', '<p>Selamat dan sukses Ananda Muhammad Raziq Arif Dermawan kelas IX Alkhawarizmi mendapatkan &quot;Juara Il &quot; PMC Indinesia 2022 Tingkat Sekolah Music Kategori Gitar<br />\r\nKlasik Junior</p>', 'I3JOvZq1f4vhnMpAEis6Y1i8HEfD9srBL3bUJdyr.jpg'),
('3e4af517-ad79-11ee-82e0-8469936849d5', 'Meisya Alexandrina', '2022-10-12', '<p>Congratulation and Proud Of Ananda Meisya Alexandrina class IX Alkhawarizmi get &quot;Bronze Medal &quot; For being a greatest participant at the english competition of Asosiasi Olimpiade Nusantara</p>', 'XwvT7HZ6MG0TvxiOGMz1ZSglbDVlJi8A1LITEM6y.jpg'),
('69fa3626-ad79-11ee-82e0-8469936849d5', 'Radhiyyah Muhammad Fahrie', '2022-10-18', '<p>Selamat dan sukses Ananda Radhiyyah Muhammad Fahrie Haqie kelas IX Alkhawarizmi mendapatkan &quot;Medali Perunggu Bidang Matematika&quot; Olimpiade Matematika dan Ilmu Pengetahuan Alam Nasional Jenjang SMP/MTs tingkat Nasional oleh Lembaga Akademi Sains Indonesia</p>', 'V5zaaYEJZ6sjlpfpJTqSKCwIKCtF0LvUCxQgStyc.jpg'),
('85215796-ad77-11ee-82e0-8469936849d5', 'Wildan Dhafi Hadya', '2022-06-15', '<p>Alhamdulillah, Selamat dan sukses atas prestasinya Wildan Dhafi Hadya kelas IX Ibnu Sina sebagai siswa berprestasi peraih &quot;Medali Perak&quot; Juara II Renang IGORNAS&quot;</p>', 'LHi1KtTJEVVFJuwr5tKgfGcVdNDEfAirqNTFTnem.jpg'),
('9030042f-ad78-11ee-82e0-8469936849d5', 'Ms Nike Wahyuni, M.Psi, Ch, cht', '2022-07-14', '<p>Selamat dan sukses atas prestasinya Ms Nike Wahyuni, M.Psi, Ch, cht guru Bimbingan Konseling SMP Al Azhar Syifa Budi Pekanbaru II meraih &quot;Juara 1&quot; sebagai Guru SMP Berprestasi Tingkat Kota Pekanbaru.</p>', 'JNeQ6Vp75ZLgUTxCdjmxrt2lX0kjhfqnitvlfuQW.jpg'),
('bd096d41-ad78-11ee-82e0-8469936849d5', 'Tengku Abyan Hanif', '2022-08-20', '<p>Selamat dan sukses atas prestasinya Tengku Abyan Hanif kelas VIII Solahudin Al Ayubi sebagai siswa berprestasi peraih &quot;Medali Emas&quot; Kategori SMP bidang Bahasa Inggris dalam kompetisi ORC-IX kompetisi Sains Tingkat &quot;Nasional&quot; Peserta Kompetisi Sains Tingkat Nasional yang diselenggarakan secara online, dikuti oleh 1081 peserta jenjang SD-SMP-SMA dari 28 provinsi di Indonesia</p>', 'dTjAvueqlhMtxSXcYYP1JYoSK95XFcDWtMn8FjMj.jpg'),
('cb5b17a7-ad79-11ee-82e0-8469936849d5', 'Shabrina Azatil Ismah, Danish Zandani Mahfousz, Bunga Tanisha', '2022-06-11', '<p>Selamat Kepada Juara Karya Tulis Ilmiah SMP AL AZHAR SYIFA BUDI PEKANBARU II Tahun 2022</p>', 'UPmo4M8NO18pd4Z0MOUc5LLOaoqUz5ZF3Q1py4rw.jpg'),
('d67d48d2-ad77-11ee-82e0-8469936849d5', 'Meisya Alexandrina', '2022-11-12', '<p>Selamat dan sukses atas prestasinya Meisya Alexandrina kelas IX AI Khawarizmi sebagai siswa berprestasi peraih &quot;Medali Emas&quot; Kategori English Smart Olimpiad&quot;<br />\r\nPeserta Kompetisi Asosiasi Olimpiade Nusantara</p>', 'jIBs0LTeZX1UCaYxGRKhuyEOc7AjWO8L6b1KQmMb.jpg'),
('e81f3987-ad78-11ee-82e0-8469936849d5', 'Daud Yudo Setiawan', '2022-09-14', '<p>Selamat dan sukses atas prestasinya Ananda Daud Yudo Setiawan kelas IX Al Fatih mendapatkan &quot;Medali Perak&quot; Peserta Nasional Islamic Competition dengan mendapatkan &#39;Medali Perak&quot; dan nilai &#39;A&#39; oleh Asosiasi Olimpiade Nusantara</p>', 'zaejILJur9ycW7JAmUjBEO1PM1xcyvV6NCA4zNK1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `profil`
--

CREATE TABLE `profil` (
  `id_profil` varchar(255) NOT NULL,
  `nama_sekolah` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `logo_header` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `profil`
--

INSERT INTO `profil` (`id_profil`, `nama_sekolah`, `logo`, `logo_header`) VALUES
('2a99b82f-ae89-11ee-aa2d-8469936849d5', 'SMP Al- Azhar Syifa Budi Pekanbaru II', '04euB9sYuhg0ZoI8zrptP15fG7BJkBYarlcOBqN0.png', 'cgOyiX2cQYvJRu7DxWgxFX3NPuNjiNOFCJ7Hx8I5.png');

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `id_program` varchar(255) NOT NULL,
  `nama_program` varchar(255) NOT NULL,
  `keterangan_program` text NOT NULL,
  `tanggal_program` date NOT NULL DEFAULT current_timestamp(),
  `foto_program` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`id_program`, `nama_program`, `keterangan_program`, `tanggal_program`, `foto_program`) VALUES
('2e142986-ad87-11ee-82e0-8469936849d5', 'Safari Rumah Qur\'an (SRQ)', '<p>Satu program baru di kelas Tahfizh SMP dan SMA Al Azhar Syifa Budi Pekanbaru Il yakni program Safari Rumah Quran. Inti dari program ini disamping niat silaturahim ke rumah-rumah siswa tahfizh, juga sebagai ajang siswa untuk memurojaah hafalan qurannya setelah selama satu bulan mereka setorkan ke guru pembimbing masing-masing. Sebagaimana yang kita ketahui bersama bahwa untuk menguatkan hafalan Quran perlu ada waktu untuk murojaah hafalan, agar apa yang telah dihafalkan tidak hilang.</p>', '2023-10-20', '4eg95BUVIIdNkmn5bFeg08lj7sZAGbc0J8RrjBAV.jpg'),
('735f3485-ad89-11ee-82e0-8469936849d5', 'Perhitungan Kotak KIS', '<p>PENGHITUNGAN KOTAK KIS<br />\r\nSudah menjadi agenda rutin di SMP dan SMA Al Azhar Syifa Budi Pekanbaru II di setiap jumat pekan kedua, yakni penghitungan kotak KIS. Dalam satu bulan siswa berinfak di rumah masing-masing lalu setelah satu bulan mereka membawa kotak infak nya ke sekolah, dihitung bersama para wali kelas. Distribusi dari hasil kotak KIS ini adalah untuk mensuport program S3D (Satu Siswa Satu Dhuafa) yakni memberi beras dan sembako untuk para dhuafa. Semoga program pembiasaan ini dapat melekat di hati para siswa sehingga akan muncul jiwa berderma, rajin dan gemar bersedekah serta senang membantu kaum dhuafa. Aamiin</p>', '2023-12-08', 'PYNbvIylrfQmYFqgBRfAW66cr0fgfZ6cZ2KBeVCX.jpg'),
('833cf5b8-94a2-11ee-bb9e-8469936849d5', 'SASISABU', 'SASISABU (Satu Siswa Satu Buku)\r\nTujuan SASISABU :\r\n1. Meningkatkan minat baca siswa-siswi SMA Al Azhar Syifa Budi Pekanbaru 2\r\n2.', '2023-12-07', 'jWAoozDinZqVP4KlhtiywnQYAQytgx0qlslJqYqw.jpg'),
('bc35f7e4-94a3-11ee-bb9e-8469936849d5', 'Mabit & Liyamul lail Bulanan', '\"Meraih Jalan Kesuksesan dengan Mendekat Kepada Penyatuh Kehidupan\"\r\nProgram yang dilakukan oleh murid kelas IX & XII SMP-SMA Al Azhar Syifa Budi Pekanbaru II', '2023-01-27', 'fxB1Gpu1aJmuciYgeWIMlSHTJliTDt5Yyga6JHcm.jpg'),
('d234a635-ad88-11ee-82e0-8469936849d5', 'Program Satu Siswa Satu Dhuafa (S3D)', '<p>MENJADI MU&#39;MIN PEDULI<br />\r\nProgram S3D di SMP dan SMA Al Azhar Syifa Budi Pekanbaru II pada intinya adalah melatih kebiasaan siswa untuk menyisihkan uang jajan nya agar bisa diberikan kepada saudara yang tak punya. Di beberapa tempat dan instansi biasanya memberikan santunan di moment tertentu saja seperti menjelang puasa atau menjelang lebaran. Tapi di SMP dan SMA Al Azhar Syifa Budi Pekanbaru II menyantuninya rutin setiap bulan, karena kami tahu bahwa mereka lapar bukan di bulan2 tertentu saja, tapi di setiap bulan mereka bingung untuk mendapatkan makan. S3D adalah program yang bertujuan menjadi mumin yang peduli. Aamiin</p>', '2023-12-19', '2eEoiUXVN7cg16mP6P5OhaCqXTCTW2ecKBHqLEE4.jpg'),
('f72c643b-94a3-11ee-bb9e-8469936849d5', 'Program I\'Tikaf Al-Qur\'an', 'Program I\'Tikaf Al-Qur\'an dilakukan oleh murid SMP-SMA Al Azhar Syifa Budi Pekanbaru II', '2022-12-10', 'V9tgPYWtz7gJmkUsHjdSoLyXfIPPxvKWrmKkCP6a.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `sejarah`
--

CREATE TABLE `sejarah` (
  `id_sejarah` varchar(255) NOT NULL,
  `keterangan` varchar(10000) NOT NULL,
  `file` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `sejarah`
--

INSERT INTO `sejarah` (`id_sejarah`, `keterangan`, `file`) VALUES
('efab0856-ab94-11ee-85e4-8469936849d5', '<p><strong>Sejarah Sekolah Al-Azhar Syifa Budi Legenda</strong></p>\r\n\r\n<p>Sekolah Al-Azhar Syifa Budi Legenda berdiri pada tahun 1997 dengan jenjang Taman Azhar (Playgroup), Taman Kanak-Kanak (TK) dan Sekolah Dasar (SD) berlokasi di sebuah ruko Jl. Bima Utama Raya Kota Legenda Tambun Selatan Bekasi Timur. Alhamdulillah, sejak tahun pelajaran 2008 &ndash; 2009 yang lalu, TK Al-Azhar Syifa Budi Legenda telah meluluskan angkatan XII. Latar belakang penyelenggaraan Sekolah Al-Azhar Syifa Budi Legenda antara lain :</p>\r\n\r\n<ol>\r\n	<li>Turut serta dalam mendidik dan menyiapkan generasi bangsa yang cerdas, pandai dan ber-Akhlak Mulia.</li>\r\n	<li>Memfasilitasi kebutuhan masyarakat muslim Kota Bekasi yang membutuhkan Sekolah Islam yang berkualitas.</li>\r\n</ol>\r\n\r\n<p>Sekolah Al-Azhar Syifa Budi Legenda diselenggarakan oleh Yayasan Mahdiati Sumbangsih Hidayah sebagai salah satu mitra dari Yayasan Syifa Budi Jakarta yang bertanggung jawab atas penyelenggaraan dan mutu seluruh Sekolah Al-Azhar Syifa Budi di Indonesia.</p>\r\n\r\n<p><strong>Tujuan Pendidikan Al-Azhar Syifa Budi Legenda</strong></p>\r\n\r\n<p>Mempersiapkan cendekiawan muslim yang bertauhid, berakhlak mulia, cakap dan terampil, percaya pada diri sendiri dan berguna bagi agama, masyarakat dan Negara Republik Indonesia serta mampu menerapkan agama Islam dan ilmu pengetahuan dalam memelihara dan meningkatkan martabat bangsa.</p>', 'iHpEnZy2HbO1Y4eAbZfo7ZXHNkh6S4b4YuTN5QIl.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id_slider` varchar(255) NOT NULL,
  `keterangan` varchar(1000) NOT NULL,
  `file` varchar(10000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id_slider`, `keterangan`, `file`) VALUES
('50d20331-adb6-11ee-8f7b-8469936849d5', '<p>SMP Al- Azhar Syifa Budi Pekanbaru II</p>', 'qYiCdFAthi7cKqF5vfxntpMRu8jf5TgEZPcsuSGK.jpg'),
('d5d57320-adb6-11ee-8f7b-8469936849d5', '<p>Guru</p>', 'YQi5O7XkTvwh49P6UQNzmOm9IOGdhIqoPSX4ZUHr.jpg'),
('db2b26d8-adb6-11ee-8f7b-8469936849d5', '<p>Osis</p>', 'NfrlChETslMD1JWlscQYoslEEZs9qGA1IklTqZ8J.jpg'),
('e45f1da8-adb6-11ee-8f7b-8469936849d5', '<p>al-azhar</p>', 'PybuqLuu58887oUHPEx0bqa981Qp7BHIwUi7nb6q.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tentang`
--

CREATE TABLE `tentang` (
  `id_tentang` varchar(255) NOT NULL,
  `telepon` varchar(255) NOT NULL,
  `instagram` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tentang`
--

INSERT INTO `tentang` (`id_tentang`, `telepon`, `instagram`, `email`, `alamat`, `facebook`) VALUES
('db16039c-9a20-11ee-a251-8469936849d5', '+62 812-6179-3054', 'smpalazharsyifabudipku', 'smpalazharsyifabudipku2020@gmail.com', 'Jl. Letjend.S.Parman No.16, Suka Mulia, Kec. Sail, Kota Pekanbaru, Riau 28127', 'Al - Azhar Syifa Budi Pekanbaru II');

-- --------------------------------------------------------

--
-- Table structure for table `ucapankepsek`
--

CREATE TABLE `ucapankepsek` (
  `id_ucapan` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `keterangan` varchar(10000) NOT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `ucapankepsek`
--

INSERT INTO `ucapankepsek` (`id_ucapan`, `nama`, `keterangan`, `foto`) VALUES
('b4a21c91-ab8e-11ee-85e4-8469936849d5', 'Kurtubi, S.Ag', '<p>Alhamdulillah, dengan mengucapkan rasa syukur kehadirat Allah SWT pada Alvinya proses kegiatan ananda di jenjang Sap berakhir sudah, kini ananda harus membiasakan diri dengan suasana baru di manapun 3 tahun perjalanan ananda di SMP Al Azhar Syifa Budi Pekanbaru II, tentu banyak kenangan yang ananda rasakan selama 3 tahun kebersamaan kita Apapun itu kenangannya kami berharap segala kenangan baik dapat ananda terus ingat, sementara hal yang tidak baik segera ananda lupakan</p>\r\n\r\n<p>Ananda sekalian, di akhir masa kebersamaan kita ada sebuah musibah yang melanda dunia hingga terasa di kota kita, yakni wabah Covid 19 Sebuah wadah yang membuat semua agenda akhir tahun ananda berubah drastis, tidak ada study tout, tidak ada UNBK, bahkan untuk acara wisuda pun dibuat dengan sederhana karena cemas dengan wabah yang melanda</p>\r\n\r\n<p>Sebagai seorang muslim yang baik kita meyakini bahwa setiap musibah yang Allah berikan kepada hambanya pasti ada kebaikan yang dapat diambil di dalamnya Dengan adanya Covid kita lebih peduli dengan kebersihan dan kesehatan, dengan Covid, manusia lebih peduli kepada sesama dan dengan Covid manusia jadi lebih baik terhadap teknologi, intinya selalu saja ada kebaikan dari setiap yang Allah takdirkan</p>\r\n\r\n<p>Wabah Covid yang melanda di akhir masa pendidikan ananda di SMP membuat angkatan ananda menjadi angkatan yang istimewa, istimewa juga karena angkatan ananda akan terus dikenang sepanjang sejarah karena covid yang melanda</p>\r\n\r\n<p>Pesan kami, tetaplah terus menjadi pribadi istimewa, istimewa bukan sekedar bergelimang harta, istimewa bukan hanya karena tercapai cita cita, tapi ananda menjadi istimewa karena akhlak terjaga, menjadi istimewa karena orang tua ridho dan bangga.</p>\r\n\r\n<p>Doakan kami selalu para guru guru yang telah sedikit memberikan ilmu yang kami miliki untuk bekal ananda, doakan agar kami senantiasa ikhlas dalam mengajar, doakan agar ilmu yang kami berikan menjadi ilmu yang bermanfaat, doakan kami agar husnul khotimah, dan cari kami andai ananda tidak menemukan kami disurga nanti, agar kita bisa kembali berkumpul di surga yang mulia, bersama manusia yang mulia yakni, Nabi Muhammad SAW</p>', 'cSMuxpfSmAa6wkepwFToJUIFwQGyRkXRnRvW4zOH.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `username`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, NULL, 'dewi@gmail.com', 'dewi', NULL, '$2y$10$SuoO.ghdbI0BtDIWNiUJUeC9z5VBWhELeUgA.7KktEj/vsTnEKJyO', NULL, '2024-01-07 15:33:20', '2024-01-07 15:33:20'),
(3, NULL, 'dyvaherza@gmail.com', 'dyvaherza', NULL, '$2y$10$rb4qiI6Yg3AF8jYbmn//ueNnkGC1.TKftfKs./eSfD13K5SJ47hom', NULL, '2024-01-07 15:36:41', '2024-01-07 15:36:41');

-- --------------------------------------------------------

--
-- Table structure for table `visimisi`
--

CREATE TABLE `visimisi` (
  `id_visimisi` varchar(255) NOT NULL,
  `visi` varchar(5000) NOT NULL,
  `misi` varchar(5000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `visimisi`
--

INSERT INTO `visimisi` (`id_visimisi`, `visi`, `misi`) VALUES
('601c2530-abd3-11ee-85e4-8469936849d5', '<p>Terwujud manusia seutuhnya (insan kamil) yang memiliki kecerdasan paripura (Intelektual, emosional, dan spiritual) menuju manusia rahmatan lil alamin</p>', '<p>Dengan bersumber pada Al-Qur an, surat Ha Mim atau fusshilat ayat 33 yang dijadikan manusia orang yang baik. Berdasarkan 3 sifat :</p>\r\n\r\n<ul>\r\n	<li>Menyeru orang lain untuk mentaati Allah</li>\r\n	<li>Beramal sholeh atau berbuat baik</li>\r\n	<li>Mengucapkan amar ma ruf nahi munkar dengan selalu memperhatikan Hablum Minallah dan Hablum Minannas</li>\r\n</ul>');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id_berita`);

--
-- Indexes for table `eskul`
--
ALTER TABLE `eskul`
  ADD PRIMARY KEY (`id_eskul`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `fasilitas`
--
ALTER TABLE `fasilitas`
  ADD PRIMARY KEY (`id_fasilitas`);

--
-- Indexes for table `galeri`
--
ALTER TABLE `galeri`
  ADD PRIMARY KEY (`id_galeri`);

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id_guru`);

--
-- Indexes for table `kalender`
--
ALTER TABLE `kalender`
  ADD PRIMARY KEY (`id_kalender`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organisasi`
--
ALTER TABLE `organisasi`
  ADD PRIMARY KEY (`id_organisasi`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD PRIMARY KEY (`id_pengumuman`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `prestasi`
--
ALTER TABLE `prestasi`
  ADD PRIMARY KEY (`id_prestasi`);

--
-- Indexes for table `profil`
--
ALTER TABLE `profil`
  ADD PRIMARY KEY (`id_profil`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`id_program`);

--
-- Indexes for table `sejarah`
--
ALTER TABLE `sejarah`
  ADD PRIMARY KEY (`id_sejarah`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id_slider`);

--
-- Indexes for table `tentang`
--
ALTER TABLE `tentang`
  ADD PRIMARY KEY (`id_tentang`);

--
-- Indexes for table `ucapankepsek`
--
ALTER TABLE `ucapankepsek`
  ADD PRIMARY KEY (`id_ucapan`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
